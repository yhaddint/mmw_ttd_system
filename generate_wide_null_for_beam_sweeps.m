clear;clc;
N = 35;
two_d_over_lambda = 2*0.00280/0.00478;
null_mid = -20;
null_half_width = 2;
null_left = null_mid - null_half_width;
null_right = null_mid + null_half_width;
beam_range = linspace(-45,45,64);

% parameter for optimization
ite_num = 50;
alpha = 0.2;

for beam_idx=1:length(beam_range)
    beam_mid = beam_range(beam_idx);
    if (beam_mid < 0) | (beam_mid >27)
        x_vec(:,1) = -two_d_over_lambda*pi*(1:N).'*sin(beam_mid/180*pi);
        final_phase_vec(:,beam_idx) = [0; x_vec(:,1)];
    else
        fprintf('Computing Beam Index %d\n',beam_idx)

        w1_vec = exp(1j*two_d_over_lambda*pi*(1:N).'*sin(beam_mid/180*pi));
        w2_vec = exp(1j*two_d_over_lambda*pi*(1:N).'*sin(null_mid/180*pi));
        w3_vec = exp(1j*two_d_over_lambda*pi*(1:N).'*sin(null_right/180*pi));
        w4_vec = exp(1j*two_d_over_lambda*pi*(1:N).'*sin(null_left/180*pi));

        epsilon = 10^(-2);
        H = zeros(N,N);
        G1 = zeros(N,N);
        G2 = zeros(N,N);
        G3 = zeros(N,N);
        HL = zeros(N,N);
        A1 = zeros(N,1);
        A2 = zeros(N,1);
        A3 = zeros(N,1);
        f = zeros(N,1);
        b = 0;
        mu1(1) = 1;
        mu2(1) = 1;
        mu3(1) = 1;

        x_vec = zeros(N, ite_num);
        x_vec(:,1) = -two_d_over_lambda*pi*(1:N).'*sin(beam_mid/180*pi);
        % try sequentially solving the problem
        for idx=1:ite_num - 1

            % H matrix in min 0.5xHx+fx
            for ii=1:N
                for jj=1:N
                    if ii==jj
                        H(ii,ii) = -2*2*real((-w1_vec(ii)*exp(1j*x_vec(ii,idx)))*...
                            (1+w1_vec'*exp(-1j*x_vec(:,idx)))+1);
                        G1(ii,ii) = 2*real((-w2_vec(ii)*exp(1j*x_vec(ii,idx)))*...
                            (1+w2_vec'*exp(-1j*x_vec(:,idx)))+1);
                        G2(ii,ii) = 2*real((-w3_vec(ii)*exp(1j*x_vec(ii,idx)))*...
                            (1+w3_vec'*exp(-1j*x_vec(:,idx)))+1);
                        G3(ii,ii) = 2*real((-w4_vec(ii)*exp(1j*x_vec(ii,idx)))*...
                            (1+w4_vec'*exp(-1j*x_vec(:,idx)))+1);
                    else
                        H(ii,jj) = -2*2*real(w1_vec(ii)*w1_vec(jj)'*...
                            exp(1j*(x_vec(ii,idx)-x_vec(jj,idx))));
                        G1(ii,jj) = 2*2*real(w2_vec(ii)*w2_vec(jj)'*...
                            exp(1j*(x_vec(ii,idx)-x_vec(jj,idx))));
                        G2(ii,jj) = 2*2*real(w3_vec(ii)*w3_vec(jj)'*...
                            exp(1j*(x_vec(ii,idx)-x_vec(jj,idx))));
                        G3(ii,jj) = 2*2*real(w4_vec(ii)*w4_vec(jj)'*...
                            exp(1j*(x_vec(ii,idx)-x_vec(jj,idx))));
                    end
                end
            end

            % f vector in min 0.5xHx+fx
            % A vector in Ax<b
            for ii=1:N
                f(ii) = -2*real(1j*w1_vec(ii)*exp(1j*x_vec(ii,idx))*(1+w1_vec'*exp(-1j*x_vec(:,idx))));
                A1(ii) = 2*real(1j*w2_vec(ii)*exp(1j*x_vec(ii,idx))*(1+w2_vec'*exp(-1j*x_vec(:,idx))));
                A2(ii) = 2*real(1j*w3_vec(ii)*exp(1j*x_vec(ii,idx))*(1+w3_vec'*exp(-1j*x_vec(:,idx))));
                A3(ii) = 2*real(1j*w4_vec(ii)*exp(1j*x_vec(ii,idx))*(1+w4_vec'*exp(-1j*x_vec(:,idx))));
            end

            % b matrix in the constraint (Ax < b)
            b1 = epsilon - abs(1 + w2_vec.'*exp(1j * x_vec(:,idx)))^2;
            b2 = epsilon - abs(1 + w3_vec.'*exp(1j * x_vec(:,idx)))^2;
            b3 = epsilon - abs(1 + w4_vec.'*exp(1j * x_vec(:,idx)))^2;

            % Using Newton SQP
            HL = H + mu1(idx) * G1 + mu2(idx) * G2 + mu3(idx) * G3;
            Psi = [f + mu1(idx) * A1 + mu2(idx) * A2 + + mu3(idx) * A3; -b1; -b2; -b3];
            J = [HL, A1, A2, A3; A1.', 0, 0, 0; A2.', 0, 0, 0; A3.', 0, 0, 0];
            
            % Newton solver
            s = inv(J)*Psi;

            mu1(idx+1) = mu1(idx) - alpha * s(end-2);
            mu2(idx+1) = mu2(idx) - alpha * s(end-1);
            mu3(idx+1) = mu3(idx) - alpha * s(end);

            d_vec = s(1:end-3);

            % QP solution and updates
        %     d_vec = quadprog(H,f,A',b);
            x_vec(:,idx+1) = x_vec(:,idx) - alpha * d_vec;
        end
        %
        w1_vec_full = [1; w1_vec];
        w2_vec_full = [1; w2_vec];
        w3_vec_full = [1; w3_vec];
        w4_vec_full = [1; w4_vec];
        
        % huristic way to determine when to stop
        x_vec_full = [zeros(1,ite_num); x_vec];
        MSE = abs(w2_vec_full.'*exp(1j*x_vec_full)).^2+...
              abs(w3_vec_full.'*exp(1j*x_vec_full)).^2+...
              abs(w4_vec_full.'*exp(1j*x_vec_full)).^2;
        [~,idx_to_pick] = min(MSE);

%         x_vec_full = [zeros(1,ite_num); x_vec];
        final_phase_vec(:,beam_idx) = [0; x_vec(:,idx_to_pick)];
    end
end
final_phase360_vec = final_phase_vec/pi*180;

%%
stop_res = -25;
theta_range = linspace(-pi/2, pi/2, 500);
gain = zeros(500,1);
% two_d_over_lambda = 1/0.55;
for theta_idx = 1:500
    theta = theta_range(theta_idx);
    arr_res = exp(1j*two_d_over_lambda*pi*(0:N).'*sin(theta))/sqrt(N+1);
    for beam_idx = 2:2:16
        gain(theta_idx, beam_idx) = arr_res.'*exp(1j*final_phase_vec(:,beam_idx+32));
    end
end

figure
plot(theta_range/pi*180, 20*log10(abs(gain)),'linewidth',2);hold on

plot(linspace(null_left,null_right,10), ones(10,1)*stop_res,'k-.','linewidth',1);hold on
plot(ones(10,1)*(null_left), linspace(stop_res,5,10),'k-.','linewidth',1);hold on
plot(ones(10,1)*(null_right), linspace(stop_res,5,10),'k-.','linewidth',1)
ylim([-30,20])
xlim([-60,60])
grid on
xlabel('Angle [deg]')
ylabel('Normalized Power Gain [dB]')
%%
my_header = {}
my_header{1} = 'beam_name'
my_header{2} = 'angle'
for i = 1:36
    my_header{i+2} = strcat('ant',num2str(i));
end
csvwrite_with_headers('myawv.csv', [round(beam_range.'), beam_range.',final_phase360_vec.'], my_header)
%%
