clear;clc;
N=16;
row = 1:N; %rand(N,1);
col = circshift(fliplr(row),1).';
H_matrix = toeplitz(col,row)
%%
F = dftmtx(N);
eig_from_DFT = diag(F'*H_matrix*F)/N;
eig_from_first_row = fft(row).';
[eig_from_DFT, eig_from_first_row]
