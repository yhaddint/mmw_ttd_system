clear;
Nt = 128;
P = 128;
Nr = 16;
BW = 3e9;
df = BW/P;
beam_width = 10/180*pi;
theta_last = 50/180*pi;
theta = 50/180*pi;
AOD_error_BM = 45/180*pi;
phi = 0;
fc = 28e9;

phi_last = 0;

arx = exp(1j*pi*(0:Nr-1).'*sin(phi_last))


A_stopband = 28; % attenuation outside mainlobe (dB)

steer_dir = theta_last;
BF_temp = get_FSM_KW_codebook( steer_dir, beam_width, Nt, A_stopband);
BF_data_tx_squint_aware = BF_temp./norm(BF_temp);

steer_dir = AOD_error_BM;
BF_temp = get_FSM_KW_codebook( steer_dir, beam_width, Nt, A_stopband);
BF_data_tx_squint_aware2 = BF_temp./norm(BF_temp);


BF_data_tx_pencil_off_grid = exp(1j*pi*(0:Nt-1).'*sin(theta_last))/sqrt(Nt);
BF_data_tx_wrong = exp(1j*pi*(0:Nt-1).'*sin(AOD_error_BM))/sqrt(Nt);


BF_data_rx = exp(1j*pi*(0:Nr-1).'*sin(phi_last));

for pp=1:P
    atx_new(:,pp) = exp(1j * pi *(1 + ((pp-1)-P/2)*df/fc) * (0:Nt-1)' * sin(theta));
    
    % wide beam with squint-robust training (off-grid accuracy)
    gain_squint_aware(pp) = BF_data_rx' * arx * atx_new(:,pp)' * BF_data_tx_squint_aware;

    % wide beam with benchmark training (on/off-grid has same accuracy)
    gain_squint_wrong_angle(pp) = BF_data_rx' * arx * atx_new(:,pp)' * BF_data_tx_squint_aware2;

    % pencil beam with squint-robust training (off-grid accuracy)
    gain_pencil_off_grid(pp) = BF_data_rx' * arx * atx_new(:,pp)' * BF_data_tx_pencil_off_grid;

    % pencil beam with squint-robust training (on/off-grid has same accuracy)
    gain_pencil_wrong(pp) = BF_data_rx' * arx * atx_new(:,pp)' * BF_data_tx_wrong;
    
    gain_TTD(pp) = BF_data_rx' * arx * atx_new(:,pp)' * atx_new(:,pp)/sqrt(Nt);

    
end
%%
figure

plot((28e9+((0:P-1)-P/2)*BW/P)/1e9,20*log10(abs(gain_pencil_off_grid)),'--','linewidth',2);hold on
plot((28e9+((0:P-1)-P/2)*BW/P)/1e9,20*log10(abs(gain_pencil_wrong)),'-o','linewidth',2,'markersize',3);hold on

plot((28e9+((0:P-1)-P/2)*BW/P)/1e9,20*log10(abs(gain_squint_aware)),':','linewidth',2);hold on
plot((28e9+((0:P-1)-P/2)*BW/P)/1e9,20*log10(abs(gain_squint_wrong_angle)),'-.','linewidth',2);hold on

plot((28e9+((0:P-1)-P/2)*BW/P)/1e9,20*log10(abs(gain_TTD)),'linewidth',2);hold on


set(gca,'FontSize',14)

grid on
xlabel('Frequency [GHz]')
ylabel('Joint Array Gain [dB]')
legend('Phased Array Pencil Beam w/ Multi-stage Beam Training',...
       'Phased Array Pencil Beam w/ Single Stage Beam Training',...
       'Phased Array Wide Beam w/ Multi-stage Beam Training',...
       'Phased Array Wide Beam w/ Single Stage Beam Training',...
       'TTD Array Pencil Beam w/ Single Stage Beam Training')
ylim([10,50])