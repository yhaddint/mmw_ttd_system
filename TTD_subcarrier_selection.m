%%
clear;clc
MCtimes = 5e2;

sc_num = 2048;
f_range = linspace(27.8, 28.2, sc_num)*1e9;
f0 = 28e9;
c_speed = 3e8;
lambda0 = c_speed/f0;
dtau_az = 1/(400e6);
M_selected_SC_num = 128;
M_selected_SC_gap = sc_num/M_selected_SC_num;
M_loaded = (0: M_selected_SC_num-1) * M_selected_SC_gap + 1;

N_az = 16;
% phase_array_cand = linspace(-pi/2,pi/2,18);
% phase_array_vec = exp(1j*pi*(0:N_az-1).'*sin(phase_array_cand)); 

SNR_num = 11;
SNR_range = linspace(-15,15,SNR_num);
for MCidx = 1:MCtimes
    theta_az = rand*2*pi/2-pi/2;
    gain_az_mag = zeros(sc_num,1);
    
    dphi_az = 2*pi*f_range*dtau_az;
    v_az_vec = exp(1j*(0:N_az-1).'*dphi_az);  
    
%     lambda = c_speed./f_range;
    h_az = exp(1j*pi*(0:N_az-1).'*sin(theta_az))/sqrt(N_az)^2;       
    gain_az_mag = v_az_vec'*h_az;
    
    subcarrier_num = sc_num;
    symbol_num = 1;

    for ss=1:symbol_num
        symbol_f = zeros(subcarrier_num,1);
        symbol_SS = 1 * sqrt(M_selected_SC_gap);
        symbol_f(M_loaded) = symbol_SS;

        symbol_f_out = symbol_f.*gain_az_mag;

    %     symbol_t = ifft(symbol_f_out);
    %     symbol_cp(time_idx,1) = [symbol_t(end-CP+1:end);symbol_t];
    end
    
    % phased array part
%     gain_phase_array = phase_array_vec'*h_az;
%     for SNR_idx = 1:SNR_num
%         SNR = SNR_range(SNR_idx);
%         RSSI_phase_array = zeros(16,16);
%         for ii=1:16
%             for cc=1:16
%                 noise_phased_array = (randn(128, 1) + 1j* randn(128, 1))/sqrt(2);
%                 RSSI_phase_array(ii,cc) = mean(abs(gain_phase_array(ii) * symbol_f((cc-1)*256+(1:128)) + noise_phased_array*sqrt(10^(-SNR/10))).^2);
%             end
%         end
%         [~,idx] = max(max(RSSI_phase_array,[],2));
%         error_phased_array(MCidx,SNR_idx) = theta_az - phase_array_cand(idx);
%     end
    
    
    % adding AWGN
    for SNR_idx = 1:SNR_num
        SNR = SNR_range(SNR_idx);
        noise = (randn(M_selected_SC_num, 1) + 1j* randn(M_selected_SC_num, 1))/sqrt(2);
        symbol_f_rx = symbol_f_out(M_loaded) + noise*sqrt(10^(-SNR/10));
        RSSI_loaded_symb = abs(symbol_f_rx).^2;

        freq_center = f_range(M_loaded);
%         freq_center = (27.8+0.4/sc_num*((0:(M_selected_SC_num-1))*M_selected_SC_gap+1))*1e9;
        angle = asin((mod(2.5e-9*2*freq_center+1,2)-1)./(freq_center./28e9));
        [~,idx] = max(RSSI_loaded_symb);
        error(MCidx,SNR_idx) = theta_az - angle(idx);
        
        % post-training beam steering
        post_BFT_vec = exp(1j*pi*(0:N_az-1).'*sin(angle(idx)));   
        post_BFT_gain(MCidx,SNR_idx) = abs(post_BFT_vec'*h_az).^2;
    end
end
%%
for SNR_idx = 1:SNR_num
    post_BFT_gain_ave(SNR_idx) = mean(post_BFT_gain(:,SNR_idx));
    error_rate(SNR_idx) = 1-(sum(abs(error(:,SNR_idx))<(105/180*pi/16))/MCtimes);
%     error_rate_phased_array(SNR_idx) = 1-(sum(abs(error_phased_array(:,SNR_idx))<(105/180*pi/16))/MCtimes);
end

figure
plot(SNR_range, error_rate,'-o','linewidth',2);hold on
% plot(SNR_range, error_rate_phased_array,'--x','linewidth',2)
set(gca,'fontsize',14)
grid on
xlabel('SNR [dB]')
ylabel('Misalignment Rate')
legend('TTD w/ 1 Rx scan')

figure
plot(SNR_range, 10*log10(post_BFT_gain_ave),'-o','linewidth',2);hold on
set(gca,'fontsize',14)
xlabel('SNR [dB]')
ylabel('Post Training Gain [dB]')
grid on

% figure
% plot(angle/pi*180, RSSI)
% grid on
% xlabel('Angle [deg]')
% ylabel('Mag')

% noise = (randn(length(symbol_cp),1)+1j*randn(length(symbol_cp),1))*10^(-3.2);
% ydata = 20*log10(abs(fft(noise + symbol_cp)));
% figure
% plot(linspace(27.8,28.2,length(ydata)),[ydata(end/2+1:end);ydata(1:end/2)])
% xlabel('Frequency [GHz]')
% ylabel('PSD [dB]')
% ylim([-30,20])
% grid on