clear;clc;
MCtimes = 1e4;
theta = (rand(MCtimes,1)*2-1)*pi/3;
theta0 = (rand(MCtimes,1)*2-1);

tau_num = 6;
dtau_range = (2.^(linspace(-3,2,tau_num)))*2.5e-9;

for dtau_index = 1:tau_num
    f_range = linspace(27.8,28.2,32*(2^dtau_index))*1e9;
    for MCindex = 1:MCtimes
        dtau = dtau_range(dtau_index);
        error(MCindex,dtau_index) = min(abs(asin(mod(2*f_range*dtau+1,2)-1) - theta(MCindex)).^2);
        error2(MCindex,dtau_index) = min(abs((mod(2*f_range*dtau+1,2)-1) - theta0(MCindex)).^2);

    end
end
%%
error_mean = sqrt(mean(error,1));
error2_mean = sqrt(mean(error2,1));
figure
plot(dtau_range/1e-9, error_mean,'-o','linewidth',2)
hold on
plot(dtau_range/1e-9, error2_mean,'-o','linewidth',2)
grid on
xlabel('dtau [ns]')
ylabel('MSE')