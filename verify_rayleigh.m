clear;clc;
%% verify the independent channel tap in frequency domain
Ts = 1e-9;
fc = 28e9;
max_delay = 100e-9 % maximum delay is 100ns
ray_num = 5e2; % number of rays
MCtimes = 5e2;
ydata = zeros(101,MCtimes);
for MCidx = 1:MCtimes
    
    xdata = linspace(0,100,101); 
    
    for ray_idx = 1:ray_num
        tau = rand * max_delay;
        ydata(:,MCidx) = ydata(:,MCidx) + exp(1j*2*pi*fc*tau) * sinc(xdata-tau/Ts)'/sqrt(ray_num);
    end
    
end
%%

figure

% emprical pdf of all taps
[b,a] = ksdensity(abs(reshape(ydata(1:100,:),100*MCtimes,1)));
plot(a,b,'linewidth',2)
hold on

% emprical pdf of tap #5
[b,a] = ksdensity(abs(ydata(5,:)));
plot(a,b,'linewidth',2)
hold on

% emprical pdf of tap #25
[b,a] = ksdensity(abs(ydata(25,:)));
plot(a,b,'linewidth',2)
hold on

% rayleight distribution with estimated parameter b_rayleigh
b_rayleigh = mean(abs(reshape(ydata(1:100,:),100*MCtimes,1)))/sqrt(pi/2);
x = linspace(0,max(a),100);
plot(x, raylpdf(x,b_rayleigh),'k--','linewidth',2)


grid on
xlabel('Mag of taps')
ylabel('PDF')
legend('Emp. PDF (all taps)','Emp. PDF (tap 25)','Emp. PDF (tap 5)','Rayleigh')
tap_corr = zeros(90,1);

% correlation 
for tap1 = 1:90
    for tap2 = tap1:90
        tap_corr(tap2-tap1+1) = tap_corr(tap2-tap1+1)...
            +abs(ydata(tap1,:)*ydata(tap2,:)'/sqrt((ydata(tap1,:)*ydata(tap1,:)' * ydata(tap2,:)*ydata(tap2,:)')))/(90-(tap2-tap1));
    end
end
figure
plot(tap_corr,'linewidth',2)
xlabel('m-n')
ylabel('corr b/w time domain taps')
grid on