clear;clc;


%% system parameters
window_length = 512;       % window size (FFT) in spectrum analysis
sampling_rate = 3.932;      % sampling rate in radio is 3.932GHz
chan_num = 4;               % Number of digital array element
N = chan_num;               % array size
% sweep_num = 200;            % number of digital beamforming angles


%% data processing of wideband signal captured in digital array 
filename1 = 'data/capture_wb_tx_no_reflector.csv';
sig_4_chan_env1_tx = loadIQfromCSV(filename1);

filename1 = 'data/capture_wb_rx_no_reflector.csv';
sig_4_chan_env1_rx = loadIQfromCSV(filename1);

filename2 = 'data/capture_wb_tx_single_reflector.csv';
sig_4_chan_env2_tx = loadIQfromCSV(filename2);

filename2 = 'data/capture_wb_rx_single_reflector.csv';
sig_4_chan_env2_rx = loadIQfromCSV(filename2);
%% run auto-correlation (get some ideal of timing offset)
% note fractional time offset is not taken care since thre is high
% oversampling rate (integer is enough)
% the results shows that the offset among 4 channels are [0,0,16,0];
sig1 = sig_4_chan_env2_rx(1,:);
sig2 = sig_4_chan_env2_rx(3,1:end);
% sig2 = sig_4_chan_env1_rx(1,1:end);
[c, lags] = xcorr(sig1, sig2);
figure
plot(lags,10*log10(abs(c)/(norm(sig1)*norm(sig2))))
grid on
xlim([-1e2,1e2])
%% time align receiver signal in 4 channels (ta means time aligned)
time_offset_rx = [0,0,16,0];
for chan_idx = 1:4
    to = time_offset_rx(chan_idx);
    sig_4_chan_env1_rx_ta(:,chan_idx) = sig_4_chan_env1_rx(chan_idx, (to+1):end-(16-to)).';
end

time_offset_rx = [0,0,0,0];
sig_4_chan_env2_rx_ta = zeros(8192,4);
for chan_idx = 1:4
    to = time_offset_rx(chan_idx);
    sig_4_chan_env2_rx_ta(:,chan_idx) = sig_4_chan_env2_rx(chan_idx, :).';
end

%% This is to first "extract" LoS signal components in the receiver
% and then apply offset calibration;
% However, it does not work well; Not use this any more

% time_offset_trx = 652;
% for chan_idx = 1:4
% %     time_offset = time_offset_trx + time_offset_rx(chan_idx);
%     sig1 = sig_4_chan_env1_tx(1,1:end-time_offset_trx-16).';
%     sig2 = sig_4_chan_env1_rx_ta(time_offset_trx+1:end, chan_idx);
%     alpha(chan_idx) = pinv(sig1)*sig2;
%     chan_LoS(:,chan_idx) = alpha(chan_idx)*sig1;
% end
% % estimate offset
% for chan_idx = 1:chan_num
%     coeff(chan_idx) = pinv(chan_LoS(:,chan_idx))*(chan_LoS(:,1));
% end

%
% sig1 = sig_4_chan_env1_tx(1,1:end-time_offset_trx-16).';
% sig1 = sig_4_chan_env1_rx_ta(:,3);
% sig2 = sig_4_chan_env1_rx_ta(:,4);
% [c, lags] = xcorr(sig1, sig2);
% figure
% plot(lags,10*log10(abs(c)/(norm(sig1)*norm(sig2))))
% grid on
% xlim([-1e2,1e2])



%% calibration (this use full signal to estimate offsetl; works best)
for chan_idx = 1:4
    sig_4_chan_env1_rx(chan_idx,:) = sig_4_chan_env1_rx(chan_idx,:) - mean(sig_4_chan_env2_rx(chan_idx,:));
end

for chan_idx = 1:chan_num
    coeff(chan_idx) = pinv(sig_4_chan_env1_rx_ta(:,chan_idx))*(sig_4_chan_env1_rx_ta(:,1));
end
%% Quick look at the PSD of received signal (all 4 channels; post calibration)

pxx = zeros(window_length, chan_num);
f1 = zeros(window_length, chan_num);
figure
for chan_idx = 1:4
%     [pxx(:,chan_idx), f1(:,chan_idx)] = pwelch(coeff(chan_idx)*sig_4_chan_env1_rx(chan_idx,:), blackman(window_length), [], window_length, sampling_rate);
    [pxx(:,chan_idx), f1(:,chan_idx)] = pwelch(coeff(chan_idx)*sig_4_chan_env1_rx_ta(:,chan_idx), blackman(window_length), [], window_length, sampling_rate);
    
    % the x-axis is from 0 to 2pi; this work around converts to -pi to pi
    % and scale to actual frequency fs = 3.9GHz
    plot([f1(window_length/2+1:end,chan_idx)-sampling_rate; f1(1:window_length/2,chan_idx)],...
        10*log10([pxx(window_length/2+1:end,chan_idx);pxx(1:window_length/2,chan_idx)]),'linewidth',2)
    hold on
end
% legend('chan1')

legend('chan1','chan2','chan3','chan4')
grid on
set(gca,'FontSize',14)
xlabel('Frequency [GHz]')
ylabel('PSD [dB]')
title('PSD of captured waveform')
%% TTD combine
delay_tap = 8;
TTD_results_len = 8176 - 3*delay_tap;
sig_TTD = zeros(TTD_results_len, 1);
sig_noTTD = zeros(8176,1);
sig_raw = zeros(8176,4);

for chan_idx = 1:4
    TTD_range = ((chan_idx-1)*delay_tap+1):(8176 - (4-chan_idx)*delay_tap);
    sig_raw(:,chan_idx) = exp(1j*pi*(chan_idx-1)*sind(-30))*coeff(chan_idx)*sig_4_chan_env1_rx_ta(:,chan_idx);
    sig_TTD = sig_TTD + sig_raw(TTD_range,chan_idx);
%     sig_TTD = sig_TTD + circshift(sig_raw(:,chan_idx),delay_tap*);
    sig_noTTD = sig_raw(:,chan_idx);
end

sig_TTD = sig_TTD

% Estimate TTD frequency filter; H(f) = Y(f) - X(f) where Y(f) is frequency
% of TTD combined signal, X(f) is any one of channel (post-calibration);
[pxx0, f0] = pwelch(sig_TTD, blackman(window_length), [], window_length, sampling_rate);
[pxxx, ff] = pwelch(sig_noTTD, blackman(window_length), [], window_length, sampling_rate);
figure
% plot([f0(window_length/2+1:end)-sampling_rate; f0(1:window_length/2)],...
%     10*log10([pxx0(window_length/2+1:end);pxx0(1:window_length/2)]),'linewidth',2)

plot([f0(window_length/2+1:end)-sampling_rate; f0(1:window_length/2)],...
    10*log10([pxx0(window_length/2+1:end);pxx0(1:window_length/2)])-...
    10*log10([pxxx(window_length/2+1:end);pxxx(1:window_length/2)]),'linewidth',2)
hold on

% legend('chan1')
grid on
set(gca,'FontSize',14)
xlabel('Frequency [GHz]')
ylabel('PSD [dB]')
title('Frequency Dependent TTD Combiner Gain')
xlim([-0.24,0.24])



%% csv loding function

function IQdata = loadIQfromCSV(filename)
    [num, str, raw] = xlsread(filename);
    rawval = double(cellfun(@(x) ~isnumeric(x), raw));
    a = zeros(size(raw));
    b = a;
    a(rawval == 0) = cell2mat(raw(rawval == 0));
    b(rawval == 1) = str2double(raw(rawval == 1));
    IQdata = a + b;
end