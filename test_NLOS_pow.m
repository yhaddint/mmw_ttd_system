clear;clc;
N = 32;
path_num = 30;
MCtimes = 1e3;
rx_pow = zeros(MCtimes,1);
for MCidx = 1:MCtimes
    w = exp(1j*pi*(0:N-1)'*sin(rand*2*pi-pi));
    theta = rand(path_num,1)*2*pi-pi;
    atx = exp(1j*pi*(0:N-1)'*sin(theta'))/sqrt(path_num*N);
    path_phases = exp(1j*rand(path_num,1)*2*pi);
    rx_pow(MCidx) = path_phases'*(atx'*w);
end
%%
[b,a] = ksdensity(real(rx_pow));
figure
plot(a,b)
hold on
[b,a] = ksdensity(imag(rx_pow));
plot(a,b)
grid on
legend('real','imag')
mean(abs(rx_pow).^2)