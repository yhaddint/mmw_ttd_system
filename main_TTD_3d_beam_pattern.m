clear;

fc_num = 30;
fc_range = linspace(27.8, 28.2, fc_num)*1e9;

f0 = 28e9;
c_speed = 3e8;
lambda0 = c_speed/f0;
d_scaling = 1;
d = lambda0/2*d_scaling;

dtau_az = 5/(400e6);% + d/c_speed*sin(0/180*pi);%*fc_num/(fc_num+1);
% dtau_az = d/c_speed*sin(45/180*pi);
% dpsi_az = pi*sin(45/180*pi)
% dtau_az = 10/f0;
dtau_el = 1/(400e6);

N_az = 4;
N_el = 4;
theta_az_num = 64;
theta_az_range = linspace(-pi/2,pi/2,theta_az_num);
theta_el_num = 64;
theta_el_range = linspace(-pi/2,pi/2,theta_el_num);

error = (rand(N_az,1)*2-1) * 0e-12;
error_freq_depen = (rand(N_az,fc_num)*2-1) * 0e-12;

for ff=1:fc_num
    fc = fc_range(ff);
    lambda = c_speed/fc;
    dphi_el = 2*pi*fc*dtau_el;
    
    array_win = kaiser(N_az,3)./norm(kaiser(N_az,3))*sqrt(N_az);
    phases_fromTTD = zeros(N_az,1);
    for kk=1:N_az
        
        phases_fromTTD(kk) = (kk-1) * 2 * pi * fc * dtau_az...
                            + 2 * pi * fc * error(kk);...
%                             + pi *(kk-1) * (0.5);%sin(0/180*pi);%...
%                             + 2 * pi * fc * error_freq_depen(kk,ff)...
%                             - 2 * pi * f0 * error(kk)...
%                             - 2 * pi * f0 * error_freq_depen(kk,5);
    end
    
    v_az_vec(:,ff) = exp(1j*phases_fromTTD);%.*array_win;
%     v_PAA_az_vec = exp(1j*(0:N_az-1).'*dpsi_az);%.*array_win;
    v_el_vec(:,ff) = exp(1j*(0:N_el-1).'*dphi_el);

    
    for tt=1:theta_az_num
        h_az(:,tt) = exp(-1j*2*pi*d/lambda*(0:N_az-1).'*...
                        sin(theta_az_range(tt)))/sqrt(N_az)^2;

        gain_az_mag(ff,tt) = v_az_vec(:,ff)'*h_az(:,tt);
        gain_az(ff,tt) = abs(gain_az_mag(ff,tt))^2;
        
%         gain_PAA_az_mag(ff,tt) = h_az(:,tt)'*v_PAA_az_vec;
%         gain_PAA_az(ff,tt) = abs(h_az(:,tt)'*v_PAA_az_vec)^2;
        
    end
    
    for tt=1:theta_el_num
        h_el(:,tt) = exp(1j*2*pi*lambda0/2/lambda*(0:N_el-1).'...
                *sin(theta_el_range(tt)))/sqrt(N_el)^2;
        gain_el(ff,tt) = abs(h_el(:,tt)'*v_el_vec(:,ff))^2;
    end
end
%%
fig = figure('Position', [150 150 900 800]);
% fc_num = 10;
nImages = fc_num;
for f_idx = 1:fc_num
    az_gain_dB = 10*log10(gain_az(f_idx,:));
    az_cloud = find(az_gain_dB>-3);
    
    el_gain_dB = 10*log10(gain_el(f_idx,:));
    el_cloud = find(el_gain_dB>-3);
    [X,Y] = meshgrid(theta_az_range(az_cloud)/pi*180, theta_el_range(el_cloud)/pi*180);
    ax1 = subplot('position',[0.3, 0.1, 0.6, 0.6]);
    plot(X,Y,'k.','linewidth',2)
%     set(h(1), 'position', [l, b, w, h])
    hold on
    h_temp = plot(X,Y,'o','linewidth',2,'MarkerEdgeColor',[   0    0.4470    0.7410]);
    set(gca,'FontSize',12)
    xlabel('Azimuth [deg]')
    ylabel('Elevation [deg]')
    grid on
    xlim([-90,90])
    ylim([-90,90])
    set(gca,'xaxisLocation','top')
    
    ax2 = subplot('position',[0.3, 0.80, 0.6, 0.15]);
    area(theta_az_range/pi*180,az_gain_dB,-30);
    set(gca,'FontSize',12)
    xlim([-90,90])
    ylim([-30,0])
    ylabel('Az Gain [dB]')
    grid on
    
    ax3 = subplot('position',[0.05, 0.1, 0.15, 0.6]);
    area(theta_el_range/pi*180,el_gain_dB,-30);
    set(gca,'FontSize',12)
    xlim([-90,90])
    ylim([-30,0])
    grid on
    ylabel('El Gain [dB]')
    camroll(90)
    
    mytext = ['Freq=', num2str(fc_range(f_idx)/1e9,'%.3f'),'GHz'];
    delete(findall(gcf,'type','annotation'))
    tbox = annotation('textbox', [0.035, 0.85, 0, 0], 'string', mytext,'FitBoxToText','on');
    tbox.FontSize = 14;
    
    % Area in this frequency is highlighted in blue (but not hold)
    
    drawnow
    frame = getframe(fig);
    im{f_idx} = frame2im(frame);
    delete(h_temp)
end
close;
filename = 'testAnimated.gif'; % Specify the output file name
for idx = 1:nImages
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',1);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.15);
    end
end
%% Analysis part
% N = 8;
N_az=8;
N_el=4;
xdata = linspace(1e-6,0.5,100);
G_az = abs(sin(N_az*pi/2*xdata)./sin(pi/2*xdata)).^2/N_az^2;
G_el = abs(sin(N_el*pi/2*xdata)./sin(pi/2*xdata)).^2/N_el^2;
[Xaz,Yel] = meshgrid(xdata,xdata);
figure
contourf(Xaz,Yel,G_az'*G_el)
colorbar
% grid on
% xlim([0,1])

%%
% fig = figure('units','inch','position',[0,0,16,4]);
% subplot(211)
% f_idx_temp = reshape((1:64).',16,4);
% f_idx = reshape(f_idx_temp(1:4,:),16,1);
f_idx = 6;

figure
polarplot(pi+theta_az_range, 10*log10(gain_az(f_idx,:)),'linewidth',3);
set(gca,'FontSize',18)
% title('TTD Pattern')

rlim([-20,0])
% thetalim([-90,90])
thetalim([90,270])
thetaticks(90:30:270)
thetaticklabels({'-90','-60','-30','0','30','60','90'})

% rlim([-10,0])
% thetalim([210,240])
% thetaticks(225)
% thetaticklabels({'45'})
title('Azimuth Pattern')
grid on
legendtext = {};
for ff=1:length(f_idx)
    legendtext{ff} = ['f',num2str(ff),'=', num2str(fc_range(f_idx(ff))/1e9,'%.2f') 'GHz'];
end

% legendtext = {'a','b'};
legend(legendtext,'Location','eastoutside','FontSize',11)


figure
polarplot(pi+theta_el_range, 10*log10(gain_el(f_idx,:)),'linewidth',3);
set(gca,'FontSize',18)
% title('TTD Pattern')

rlim([-20,0])
% thetalim([-90,90])
thetalim([90,270])
thetaticks(90:30:270)
thetaticklabels({'-90','-60','-30','0','30','60','90'})

% rlim([-10,0])
% thetalim([210,240])
% thetaticks(225)
% thetaticklabels({'45'})
title('Elevation Pattern')
grid on
legendtext = {};
for ff=1:length(f_idx)
    legendtext{ff} = ['f',num2str(ff),'=', num2str(fc_range(f_idx(ff))/1e9,'%.2f') 'GHz'];
end

% legendtext = {'a','b'};
legend(legendtext,'Location','eastoutside','FontSize',11)

