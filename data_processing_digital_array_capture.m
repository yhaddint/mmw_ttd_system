clear;clc;

%%
% Load the data; CSV file already contains complex I/Q samples
% Each data with dimension 4 (# of I/Q ADCs) by 8192 (# of samples)

filename1 = 'data/capture_tone_no_reflector.csv';
sig_4_chan_env1 = loadIQfromCSV(filename1);

filename2 = 'data/capture_tone_single_reflector.csv';
sig_4_chan_env2 = loadIQfromCSV(filename2);

%% system parameters
window_length = 1024;       % window size (FFT) in spectrum analysis
sampling_rate = 3.932;      % sampling rate in radio is 3.932GHz
chan_num = 4;               % Number of digital array element
N = chan_num;               % array size
sweep_num = 200;            % number of digital beamforming angles

%% The received waveform is a tone signal at +240MHz baseband;
% The plot shows tone in frequency domain

pxx = zeros(window_length, chan_num);
f1 = zeros(window_length, chan_num);
figure
for chan_idx = 1:chan_num
    [pxx(:,chan_idx), f1(:,chan_idx)] = pwelch(sig_4_chan_env1(chan_idx,:), blackman(window_length), [], window_length, sampling_rate);
    
    % the x-axis is from 0 to 2pi; this work around converts to -pi to pi
    % and scale to actual frequency fs = 3.9GHz
    plot([f1(window_length/2+1:end,chan_idx)-sampling_rate; f1(1:window_length/2,chan_idx)],...
        10*log10([pxx(window_length/2+1:end,chan_idx);pxx(1:window_length/2,chan_idx)]),'linewidth',2)
    hold on
end
legend('chan1','chan2','chan3','chan4')
grid on
set(gca,'FontSize',14)
xlabel('Frequency [GHz]')
ylabel('PSD [dB]')
title('PSD of captured waveform')
%% The array is not calibrated; 
% pinv() is used to estimate single complex value of gain/phase offset

for chan_idx = 1:chan_num
    coeff(chan_idx) = pinv(sig_4_chan_env1(chan_idx,:)')*(sig_4_chan_env1(1,:)');
end
% figure
% plot(coeff,'o','linewidth',2)
% grid on
% xlabel('Real')
% ylabel('Imag')
% xlim([-2,2])
% ylim([-2,2])
%% Digital beamforming;
% Using captured data to estimate receiver combining gain with different steering
% directions

psd_max = zeros(sweep_num,1);                                % initialize vector
angle_range = linspace(-pi/2, pi/2, sweep_num);              % beam sweep candidates angles
awv_sweep = exp(-1j*pi*(0:N-1).'*sin(angle_range));          % beamformer for sweep (pre-offset-compensation)
awv_sweep_post_cal = diag(coeff.')*awv_sweep;                % beamformer for sweep (post-offset-compensation)
sig_comb_env1 = awv_sweep_post_cal'*sig_4_chan_env1;         % apply beamformer to data
sig_comb_env2 = awv_sweep_post_cal'*sig_4_chan_env2;         % apply beamformer to data

for sweep_idx = 1:sweep_num
    [psd_env1, sig_freq] = pwelch(sig_comb_env1(sweep_idx,:), blackman(window_length), [], window_length, sampling_rate);
    [psd_env2, sig_freq] = pwelch(sig_comb_env2(sweep_idx,:), blackman(window_length), [], window_length, sampling_rate);

    psd_max_env1(sweep_idx) = max(10*log10(psd_env1));       % BF gain is evaluated at peak tone (240MHz)
    psd_max_env2(sweep_idx) = max(10*log10(psd_env2));

end
%% Theoretical results of combining gain in ideal 4-element linear array

array_res = ones(N, 1);                                       % boresight array response
array_gain_sim = 20*log10(abs(awv_sweep'*array_res));         % using pre-offset-compensated AWV

%% plot beam pattern
figure
plot(angle_range/pi*180, psd_max_env1 - max(psd_max_env1),'linewidth',2)
hold on
plot(angle_range/pi*180, psd_max_env2 - max(psd_max_env2),'linewidth',2)
hold on
plot(angle_range/pi*180, array_gain_sim - max(array_gain_sim),'k--','linewidth',1);
grid on
set(gca,'FontSize',14)
ylim([-20,0])
xlabel('Steering Angle [deg]')
ylabel('Normalized Array Gain [dB]')
legend('Data (LoS)','Data (LoS + Reflector)','Theo (LoS)')
title('combining gain at different angles')



%% csv loding function

function IQdata = loadIQfromCSV(filename)
    [num, str, raw] = xlsread(filename);
    rawval = double(cellfun(@(x) ~isnumeric(x), raw));
    a = zeros(size(raw));
    b = a;
    a(rawval == 0) = cell2mat(raw(rawval == 0));
    b(rawval == 1) = str2double(raw(rawval == 1));
    IQdata = a + b;
end
