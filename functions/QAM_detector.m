function [det_symb] = QAM_detector(rx_symb)
    %QAM_DETECTOR1 Summary of this function goes here
    %   Detailed explanation goes here
    [row_num, col_num] = size(rx_symb);
    det_symb = zeros(row_num, col_num);
    for row = 1:row_num
        for col = 1:col_num
            if real(rx_symb(row,col))>0 & imag(rx_symb(row,col))>0
                det_symb(row,col) = 1+1j;
            elseif real(rx_symb(row,col))>0 & imag(rx_symb(row,col))<0
                det_symb(row,col) = 1-1j;
            elseif real(rx_symb(row,col))<0 & imag(rx_symb(row,col))>0
                det_symb(row,col) = -1+1j;
            elseif real(rx_symb(row,col))<0 & imag(rx_symb(row,col))<0
                det_symb(row,col) = -1-1j;
            end
        end
    end
end

