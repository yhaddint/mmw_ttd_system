function [RB_idx,layer_idx] = findRB(AoA,RB_LUT,RB_inband,K,RB)
    det_flag = find(abs(RB_LUT(RB_inband,:)-AoA)<1.01/RB);
    RB_idx = mod(det_flag(1)-1,RB/K) + RB_inband(1);
    layer_idx = floor((det_flag(1)-1)/(RB/K))+1;
end
