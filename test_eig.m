clear;clc;
N = 32;
Z = zeros(N,N);
MCtimes = 1e3;
for MCidx = 1:MCtimes
    phi = rand*pi-pi/2;
    a = exp(1j*(0:N-1)'*pi*sin(phi));
    Z = Z+a*a';
end
[V,D] = eig(Z);