clear;clc;
N = 512;
N_b = 8;
s_t = randn(N,1);
s_f = dftmtx(N)*s_t;
s_f_missing = s_f;
s_f_missing(1:N_b:end) = 0;
s_t_missing = dftmtx(N)'*s_f_missing;
s_t_new = reshape(s_t,N/N_b,N_b);
s_t_new_sum = sum(s_t_new,2);
figure
subplot(211)
plot(real(dftmtx(N/N_b)*s_t_new_sum),'-')
hold on
plot(real(s_f(1:N_b:end)),'o')
legend('filled','original')
grid on
subplot(212)
plot(imag(dftmtx(N/N_b)*s_t_new_sum),'--')
hold on
plot(imag(s_f(1:N_b:end)),'x')
legend('filled','original')
grid on