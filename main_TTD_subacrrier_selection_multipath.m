%%
clear;clc;
MCtimes = 1e2;

TTD_half_scan = 0;

channel_model = 'quadriga';                 % 'quadriga' or 'sv'

Ts = 1/400e6;
fc = 28e9;                                  % carrier freq [Hz]
P = 2048;                                   % subcarrier number
sc_num = P;
Nr    = 16;                                 % Num of antenna in UE/Rx (total)
Nr_az = 16;                                  % Num of antenna in UE/Rx (azimuth)
Nr_el = 1;                                  % Num of antenna in UE/Rx (elevation)

Nt    = 64;                                 % Num of antenna in BS/Tx (total)                           
Nt_az = 64;                                  % Num of antenna in BS/Tx (azimuth) 
Nt_el = 1;                                 % Num of antenna in BS/Tx (elevation) 

f_range = linspace(27.8, 28.2, sc_num)*1e9;
f0 = 28e9;
RB_len = 12;                             % length of resource block
c_speed = 3e8;                              % speed of light
lambda0 = c_speed/f0;
dtau_az = 1/(400e6);                     % TTD delay difference
M_selected_SC_num = 32;                     % num of selected SC (RB in fact)
M_selected_SC_gap = sc_num/M_selected_SC_num;
M_loaded = (0: M_selected_SC_num-1) * M_selected_SC_gap + 1;
M_loaded_center = M_loaded + RB_len/2;
M_loaded_all = reshape(repmat((M_loaded-1),RB_len,1) + repmat((1:RB_len)',1,M_selected_SC_num),M_selected_SC_num*RB_len,1);
PAA_scan_saving = [1,2,4,8];                  % number of OFDM symb used (reduction ratio to baseline
if TTD_half_scan == 0
    TTD_symb_num = 3;
elseif TTD_half_scan == 1
    TTD_symb_num = 4;
end

AOAspread_az = 0/180*pi;                 % Intra-cluster AoA spread square 
cluster_num = 1;
path_num = 20;
Ts = 1/400e6;

PAA_scan_times = 32;                    % PAA baseline sweep number
PAA_DFT_beam_or_uniform_angle = 'DFT';
% PAA uniform angle scan or DFT beam
switch PAA_DFT_beam_or_uniform_angle
    case 'uniform_angle'
        phase_array_cand = linspace(-pi/2,pi/2,PAA_scan_times);
        phase_array_vec = exp(1j*pi*(0:Nr_az-1).'*sin(phase_array_cand)); 
    case 'DFT'
        phase_array_cand = linspace(-1,1,PAA_scan_times);
        phase_array_vec = exp(1j*pi*(0:Nr_az-1).'*phase_array_cand); 
end

SNR_num = 7;
SNR_range = linspace(-30,10,SNR_num);
post_BFT_rate_PAA = zeros(MCtimes, SNR_num, length(PAA_scan_saving));
post_BFT_gain_PAA = zeros(MCtimes, SNR_num, length(PAA_scan_saving));
error_PAA = zeros(MCtimes, SNR_num, length(PAA_scan_saving));

for MCidx = 1:MCtimes
    clc;fprintf('Iteration %d',MCidx)
    theta0_az(MCidx) = rand * 2 * pi/3 - pi/3;
    gain_az_mag = zeros(sc_num,1);
    gain_az_mag_loaded = zeros(M_selected_SC_num,1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %        QuaDRiGa Parameter and Setup
    %     It uses QuaDRiGa Tutorial 4.3 as baseline
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Include QuaDRiGa source folder
    addpath('C:\Users\Han\Documents\QuaDriGa_2017.08.01_v2.0.0-664\quadriga_src');
    
    % Fonts setting
    % set(0,'defaultTextFontSize', 18)                        % Default Font Size
    % set(0,'defaultAxesFontSize', 18)                        % Default Font Size
    % set(0,'defaultAxesFontName','Times')                    % Default Font Type
    % set(0,'defaultTextFontName','Times')                    % Default Font Type
    % set(0,'defaultFigurePaperPositionMode','auto')          % Default Plot position
    % set(0,'DefaultFigurePaperType','<custom>')              % Default Paper Type
    % set(0,'DefaultFigurePaperSize',[14.5 7.3])              % Default Paper Size
    
    s = qd_simulation_parameters;                           % New simulation parameters
    s.center_frequency = fc;                                % 28 GHz carrier frequency
    s.sample_density = 4;                                   % 4 samples per half-wavelength
    s.use_absolute_delays = 0;                              % Include delay of the LOS path
    s.show_progress_bars = 0;                               % Disable progress bars
    
    % Antenna array setting
    l = qd_layout( s );                                     % New QuaDRiGa layout
    l.tx_array = qd_arrayant('3gpp-mmw',Nt_el,Nt_az,fc,1,0,0.5,1,1);
    l.rx_array = qd_arrayant('3gpp-mmw',Nr_el,Nr_az,fc,1,0,0.5,1,1);
    % l.rx_array.rotate_pattern(180, 'z');
    % l.rx_array.visualize(1);pause(1)
    l.track = qd_track('linear',1,-pi);

    % BS and UE location 
    UE_dist = 20;                                            % BS/UE distance in x-axis
    UE_height = rand*20;                                     % Height of UE (up to 50m)
    BS_UE_pos_angle = (rand*90-45)/180*pi;
    x_UE = UE_dist * cos(BS_UE_pos_angle);
    %     y_UE = UE_dist * sin(BS_UE_pos_angle);
    BS_height = 10;                                          % Height of BS
    y_UE = rand * (2 * UE_dist * tan(45/180*pi))...
        - (UE_dist * tan(45/180*pi));                        % BS/UE dist in y axis(ramdom)
    l.tx_position(:,1) = [0,0,BS_height].';                  % BS position
    l.rx_position(:,1) = [UE_dist,y_UE,UE_height].';         % UE position
    l.set_scenario('mmMAGIC_UMi_NLOS');                      % Set propagation scenario
    %     l.visualize;                                           % Plot the layout

    % Call builder to generate channel parameter
    cb = l.init_builder;  
    
    % Some customized setting (for debuging)
    % cb.scenpar.PerClusterDS = 0;                            % Create new builder object
    %     cb.scenpar.SF_sigma = 0;                                % 0 dB shadow fading
    %     cb.scenpar.KF_mu = 0;                                   % 0 dB K-Factor
    %     cb.scenpar.KF_sigma = 0;                                % No KF variation
    %     cb.scenpar.SubpathMethod = 'mmMAGIC';
    %     cb.plpar = [];                                          % Disable path loss model

    % Call builder for small scale fading parameters
    cb.gen_ssf_parameters;                                  % Generate large- and small-scale fading
    
    % delete paths coming from opposite direction
%     for kk=1:length(cb.AoA)
%         if cb.AoA(kk)<pi/2 && cb.AoA(kk)>-pi/2
%             cb.pow(kk) = 0.5e-4;
%         end
%     end

    % Drifting model for channel dynamics (off)
    % s.use_spherical_waves = 1;                              % Enable drifting (=spherical waves)
    % c = cb.get_channels;                                    % Generate channel coefficients
    % c.individual_delays = 0;                                % Remove per-antenna delays

    % Call builder to generate channel 
    s.use_spherical_waves = 0;                              % Disable drifting
    d = cb.get_channels;                                    % Generate channel coefficients

    % Rearrange since QuaDRiGa uses different az/el order
    % each column in MIMO is [el1; el2; etc]
    % while ours is [az1, az2, etc].'
    chan_coeff = zeros(Nr,Nt,cb.NumClusters,2);
    for c_idx = 1:cb.NumClusters
        chan_MIMO_old = squeeze(d.coeff(:,:,c_idx,1));

        row_order = repmat(1:Nr_el:Nr,1,Nr_el) + kron((0:Nr_el-1),ones(1,Nr_az));
        col_order = repmat(1:Nt_el:Nt,1,Nt_el) + kron((0:Nt_el-1),ones(1,Nt_az));

        chan_MIMO_new1 = chan_MIMO_old(:,col_order);
        chan_MIMO_new2 = chan_MIMO_new1(row_order,:);

        chan_coeff(:,:,c_idx,1) = chan_MIMO_new2;

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %        Convert into Channel Taps
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    H_QDG_f = zeros(Nr,Nt,P);
    H_QDG_t = zeros(Nr,Nt,P);
    frac_delay = 10e-9;

    for pp=1:P
        for path_idx = 1:cb.NumClusters

        H_QDG_f(:,:,pp) = H_QDG_f(:,:,pp) +...
            exp(-1j*2*pi*(frac_delay + d.delay(path_idx,1))*(pp-1)/(Ts*P))...
            *squeeze(chan_coeff(:,:,path_idx,1));

        end
    end
    
    atx_angle_cand = linspace(-pi/2,pi/2,120);
    Atx = exp(1j*pi*(0:Nt-1).'*atx_angle_cand);
    for pp=1:P
        post_tx_chan(pp,:) = mean(abs(squeeze(H_QDG_f(:,:,pp)) * Atx),1);
    end
    [~,best_tx_idx] = max(mean(post_tx_chan,1));
    best_tx_beam = exp(1j*pi*(0:Nt-1).'*atx_angle_cand(best_tx_idx));
    for pp = 1:P
        post_best_tx_beam_chan(:,:,pp) = squeeze(H_QDG_f(:,:,pp))*best_tx_beam;
    end
    
        
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %        In-House S-V Channel
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    c_tau = 0.01; % 23 % intra-cluster delay spread with unit [ns]
    r_tau = 2.03; % parameter in mmMAGIC
    relative_delay_prime = -r_tau*c_tau*log(rand(path_num,1));
    relative_delay = sort(relative_delay_prime) - min(relative_delay_prime);
    delay_pow_scling = exp(-relative_delay*(r_tau-1)./(r_tau*c_tau));
    ray_delay = (relative_delay+10)*1e-9;
    theta_az = theta0_az(MCidx) + laprnd(path_num, 1, 0, AOAspread_az);
    rayAOA_az = theta_az;
    rayAOA_el = zeros(path_num,1);
    rayAOD_az = zeros(path_num,1);
    rayAOD_el = zeros(path_num,1);
    g_ray = exp(1j*rand(path_num,1)*2*pi) .* delay_pow_scling;

    
    h_az = zeros(Nr_az, 1);
    [H_chan_WB,~] = get_H_WB_3D(g_ray.',...
                        ray_delay.',...
                        rayAOA_az.',...
                        rayAOA_el.',...
                        rayAOD_az.',...
                        rayAOD_el.',...
                        cluster_num,...        % cluster number
                        path_num,...           % ray number
                        1, 1,...
                        Nr_az, 1,...
                        Ts,sc_num);
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %          Channel selection and normalization
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    switch channel_model
        case 'quadriga'
%             H_chan_WB_sel = H_QDG_f./norm(squeeze(H_QDG_f))*sqrt(P*Nr_az);
            H_chan_WB_sel = post_best_tx_beam_chan./norm(squeeze(post_best_tx_beam_chan))*sqrt(P*Nr_az);

            if cb.AoA(1)>pi/2
                true_LOS = cb.AoA(1) - pi;
            elseif cb.AoA(1)<-pi/2
                true_LOS = cb.AoA(1) + pi;
            end
            theta0_az(MCidx) = true_LOS;
        case 'sv'
            H_chan_WB_sel = H_chan_WB./norm(squeeze(H_chan_WB))*sqrt(P*Nr_az);
    end
    
    H_chan_WB_ordered = H_chan_WB_sel(:,:,[sc_num/2+1:end, 1:sc_num/2]);
    H_chan_WB_selected = H_chan_WB_ordered(:,:,M_loaded_all);
    H_chan_WB_norm = squeeze(H_chan_WB_selected);
    

    % symbol power scaling
    symbol_f = zeros(M_selected_SC_num*RB_len,1);
    symbol_SS = sqrt(sc_num/(M_selected_SC_num*RB_len));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %          PAA performance evaluation
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for tt = 1:PAA_scan_times
        noise_baseline = (randn(M_selected_SC_num*RB_len, Nr_az) + 1j* randn(M_selected_SC_num*RB_len, Nr_az))/sqrt(2);
        noise_freq_PAA(:,tt) = noise_baseline * phase_array_vec(:,tt);
    end
    gain_PAA = phase_array_vec'*H_chan_WB_norm;
    for SNR_idx = 1:SNR_num
        SNR = SNR_range(SNR_idx);
        sig_rec = gain_PAA * symbol_SS;
        sig_rec_awgn = sig_rec * sqrt(10^(SNR/10)) + noise_freq_PAA';
        RSSI_PAA = mean(abs(sig_rec_awgn).^2, 2);
        
        for save_idx=1:length(PAA_scan_saving)
            [~,idx] = max(RSSI_PAA(1:PAA_scan_saving(save_idx):end));
            PAA_cand_save = phase_array_cand(1:PAA_scan_saving(save_idx):end);
            error_PAA(MCidx,SNR_idx,save_idx) = theta0_az(MCidx) - PAA_cand_save(idx);
        
            % post-training beam steering
            switch PAA_DFT_beam_or_uniform_angle
                case 'uniform_angle'
                    post_BFT_PAA = exp(1j*pi*(0:Nr_az-1).'*sin(PAA_cand_save(idx)))/sqrt(Nr_az); 
                case 'DFT'
                    post_BFT_PAA = exp(1j*pi*(0:Nr_az-1).'*PAA_cand_save(idx))/sqrt(Nr_az); 
            end
            for sc_idx = 1:sc_num
                post_gain_PAA(sc_idx) = abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_PAA)^2/...
                    norm(squeeze(H_chan_WB_ordered(:,:,sc_idx)))^2;
                rate_PAA(sc_idx) = log2(1+10^(SNR/10)*abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_PAA)^2);
            end
            post_BFT_rate_PAA(MCidx,SNR_idx,save_idx) = mean(rate_PAA);
            post_BFT_gain_PAA(MCidx,SNR_idx,save_idx) = mean(post_gain_PAA);
        end
    end
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %          TDD performacen in AWGN
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % TTD with complete scan (freq. diversity by CB rotation)
    if TTD_half_scan ~= 1
        PS_phi_vec(1) = 0;
        PS_phi_vec(2) = pi * (-0.4995);
        PS_phi_vec(3) = pi * (-0.9995);
    end
    
    % TTD with half scan (completion and freq. diversity by CB rotation)
    if TTD_half_scan == 1
        PS_phi_vec(1) = pi * (-0.5);
        PS_phi_vec(2) = pi * (0.5);
        PS_phi_vec(3) = pi * (0);
        PS_phi_vec(4) = pi * (1);
    end
    
    % for loop for SNR sweep
    for SNR_idx = 1:SNR_num
        SNR = SNR_range(SNR_idx);
        
        % for loop for symb (codebook rotation)
        for OFDM_odx = 1:TTD_symb_num
            noise = (randn(Nr_az, sc_num) + 1j* randn(Nr_az, sc_num))/sqrt(2);
            dphi_az = 2*pi*f_range*dtau_az + repmat(PS_phi_vec(OFDM_odx),1,sc_num);
            v_az_vec = exp(1j*(0:Nr_az-1).'*dphi_az);
            
            for ii=1:length(M_loaded_all)
                gain_az_mag_loaded(ii) = squeeze(v_az_vec(:,M_loaded_all(ii)))'*H_chan_WB_norm(:,ii);
            end
            
            symbol_TTD_out = symbol_SS.*gain_az_mag_loaded;
            symbol_f_rx = zeros(length(M_loaded),1);
            for ii=1:length(M_loaded)
                noise_scaled = noise(:,M_loaded(ii):M_loaded(ii)+11);
                post_array_noise = sum(v_az_vec(:,M_loaded(ii):M_loaded(ii)+11).*noise_scaled,1);
                symbol_f_rx(ii) = mean(abs(symbol_TTD_out((ii-1)*RB_len+1:ii*RB_len) * sqrt(10^(SNR/10))+ post_array_noise.').^2);
            end
            RSSI_loaded_symb(:,OFDM_odx) = symbol_f_rx;
        end
        
        if TTD_half_scan == 1
            RSSI_loaded_symb_shuffle(:,1) = [RSSI_loaded_symb(:,1); zeros(32,1)];
            RSSI_loaded_symb_shuffle(:,2) = circshift([RSSI_loaded_symb(:,2); zeros(32,1)], 32);
            RSSI_loaded_symb_shuffle(:,3) = circshift([RSSI_loaded_symb(:,3); zeros(32,1)], 16);
            RSSI_loaded_symb_shuffle(:,4) = circshift([RSSI_loaded_symb(:,4); zeros(32,1)], -16);
        else
            RSSI_loaded_symb_shuffle(:,1) = RSSI_loaded_symb(:,1);
            RSSI_loaded_symb_shuffle(:,2) = circshift(RSSI_loaded_symb(:,2), -8);
            RSSI_loaded_symb_shuffle(:,3) = circshift(RSSI_loaded_symb(:,3), -16);
        end
        
%         figure
%         plot(RSSI_loaded_symb_shuffle(:,1));hold on
%         plot(RSSI_loaded_symb_shuffle(:,2));hold on
%         grid on
        if TTD_half_scan == 1
            [~,idx] = max(RSSI_loaded_symb_shuffle(:,1));
            mean2 = mean(RSSI_loaded_symb_shuffle(:,1:2),2);
            [~,idx_div2] = max(mean2);
            mean3 = mean(RSSI_loaded_symb_shuffle(:,1:4),2);
            [~,idx_div3] = max(mean3);
        else
            [~,idx] = max(RSSI_loaded_symb(:,1));
            [~,idx_div2] = max(mean(RSSI_loaded_symb_shuffle(:,1:2),2));
            [~,idx_div3] = max(mean(RSSI_loaded_symb_shuffle(:,1:3),2));
        end
        
        freq_center = f_range(M_loaded_center);
        if TTD_half_scan == 1
            angle1 = asin((mod(1.25e-9*2*freq_center+1-0.5,2)-1)); 
            angle2 = asin((mod(1.25e-9*2*freq_center+1+0.5,2)-1));  
            angle_list = [angle1,angle2];
        else
            angle_list = asin((mod(2.5e-9*2*freq_center+1,2)-1));
        end
        error(MCidx,SNR_idx) = theta0_az(MCidx) - angle_list(idx);
        error_div2(MCidx,SNR_idx) = theta0_az(MCidx) - angle_list(idx_div2);
        error_div3(MCidx,SNR_idx) = theta0_az(MCidx) - angle_list(idx_div3);

        
        % post-training beam steering
        post_BFT_vec = exp(1j*pi*(0:Nr_az-1).'*sin(angle_list(idx)))/sqrt(Nr_az); 
        post_BFT_vec_div2 = exp(1j*pi*(0:Nr_az-1).'*sin(angle_list(idx_div2)))/sqrt(Nr_az); 
        post_BFT_vec_div3 = exp(1j*pi*(0:Nr_az-1).'*sin(angle_list(idx_div3)))/sqrt(Nr_az); 
        
        for sc_idx = 1:sc_num
            post_gain(sc_idx) = abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_vec)^2/...
                                norm(squeeze(H_chan_WB_ordered(:,:,sc_idx)))^2;
            rate(sc_idx) = log2(1 + 10^(SNR/10)*abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_vec)^2);
            
            
            post_gain_div2(sc_idx) = abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_vec_div2)^2/...
                                norm(squeeze(H_chan_WB_ordered(:,:,sc_idx)))^2;
            rate_div2(sc_idx) = log2(1 + 10^(SNR/10)*abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_vec_div2)^2);
            
            
            post_gain_div3(sc_idx) = abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_vec_div3)^2/...
                                norm(squeeze(H_chan_WB_ordered(:,:,sc_idx)))^2;
            rate_div3(sc_idx) = log2(1 + 10^(SNR/10)*abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_vec_div3)^2);
        end
        post_BFT_rate(MCidx,SNR_idx) = mean(rate);
        post_BFT_gain(MCidx,SNR_idx) = mean(post_gain);
        
        post_BFT_rate_div2(MCidx,SNR_idx) = mean(rate_div2);
        post_BFT_gain_div2(MCidx,SNR_idx) = mean(post_gain_div2);
        
        post_BFT_rate_div3(MCidx,SNR_idx) = mean(rate_div3);
        post_BFT_gain_div3(MCidx,SNR_idx) = mean(post_gain_div3);
    end
    
    % estimation of covariance (left for future)
%     covmtx = zeros(16,16);
%     for ii=1:2048
%         xxx = squeeze(H_chan_WB_ordered(:,:,ii));
%         covmtx = covmtx + xxx * xxx';
%     end
%     [U, Sigma, V] = svd(covmtx);
% 
%     for sc_idx = 1:sc_num
%         post_gain_cov(sc_idx) = abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*V(:,1))^2/...
%             norm(squeeze(H_chan_WB_ordered(:,:,sc_idx)))^2;
%         rate_cov(sc_idx) = log2(1 + 10^(SNR/10)*abs(squeeze(H_chan_WB_ordered(:,:,sc_idx))'*post_BFT_vec)^2);
%     end
%     
%     post_BFT_rate_cov(MCidx) = mean(rate_cov);
%     post_BFT_gain_cov(MCidx) = mean(post_gain_cov); 
%     
%     fprintf('\ncov = %.2f dB, TTD = %.2f\n dB', 10*log10(post_BFT_gain_cov(MCidx)),10*log10(post_BFT_gain(MCidx,end)));

end
%%
for SNR_idx = 1:SNR_num
    post_BFT_rate_ave(SNR_idx) = mean(post_BFT_rate(:,SNR_idx));
    post_BFT_gain_ave(SNR_idx) = mean(post_BFT_gain(:,SNR_idx));
    error_rate(SNR_idx) = 1-(sum(abs(error(:,SNR_idx))<(105/2/180*pi/Nr_az))/MCtimes);
    
    post_BFT_rate_ave_div2(SNR_idx) = mean(post_BFT_rate_div2(:,SNR_idx));
    post_BFT_gain_ave_div2(SNR_idx) = mean(post_BFT_gain_div2(:,SNR_idx));
    error_rate_div2(SNR_idx) = 1-(sum(abs(error_div2(:,SNR_idx))<(105/2/180*pi/Nr_az))/MCtimes);
    
    post_BFT_rate_ave_div3(SNR_idx) = mean(post_BFT_rate_div3(:,SNR_idx));
    post_BFT_gain_ave_div3(SNR_idx) = mean(post_BFT_gain_div3(:,SNR_idx));
    error_rate_div3(SNR_idx) = 1-(sum(abs(error_div3(:,SNR_idx))<(105/2/180*pi/Nr_az))/MCtimes);

    
    for save_idx = 1:length(PAA_scan_saving)
        post_BFT_rate_ave_PAA(SNR_idx,save_idx) = mean(squeeze(post_BFT_rate_PAA(:,SNR_idx,save_idx)));
        post_BFT_gain_ave_PAA(SNR_idx,save_idx) = mean(squeeze(post_BFT_gain_PAA(:,SNR_idx,save_idx)));
        error_rate_PAA(SNR_idx,save_idx) = 1-(sum(abs(squeeze(error_PAA(:,SNR_idx,save_idx)))<(105/180*pi/Nr_az))/MCtimes);
    end
end

figure
plot(SNR_range, error_rate,'-o','linewidth',2);hold on
plot(SNR_range, error_rate_div2,'-o','linewidth',2);hold on
plot(SNR_range, error_rate_div3,'-o','linewidth',2);hold on

% plot(SNR_range, error_rate_PAA,'--x','linewidth',2)
set(gca,'fontsize',14)
grid on
xlabel('SNR [dB]')
ylabel('Misalignment Rate')
legend('TTD, K_B_T=1','TTD, K_B_T=2','TTD, K_B_T=3')

% legend('TTD, K_B_T=1','TTD, K_B_T=2','TTD, K_B_T=3','PAA, K_B_T=32','PAA, K_B_T=16','PAA, K_B_T=8','PAA, K_B_T=4')

figure
plot(SNR_range, 10*log10(post_BFT_gain_ave),'-o','linewidth',2);hold on
% plot(SNR_range, 10*log10(post_BFT_gain_ave_div2),'-s','linewidth',2);hold on
% plot(SNR_range, 10*log10(post_BFT_gain_ave_div3),'-o','linewidth',2);hold on
plot(SNR_range, 10*log10(post_BFT_gain_ave_PAA),'--x','linewidth',2);hold on
% plot(SNR_range, 10*log10(post_BFT_gain_cov),'-k','linewidth',2);hold on

set(gca,'fontsize',14)
xlabel('SNR [dB]')
ylabel('Post Training Gain [dB]')
% legend('TTD, K_B_T=1','TTD, K_B_T=2','TTD, K_B_T=3')
% legend('TTD, K_B_T=1','TTD, K_B_T=2','TTD, K_B_T=3','PAA, K_B_T=32','PAA, K_B_T=16','PAA, K_B_T=8','PAA, K_B_T=4')
legend('TTD, K_B_T=1','PAA, K_B_T=32','PAA, K_B_T=16','PAA, K_B_T=8','PAA, K_B_T=4')
grid on


figure
plot(SNR_range, post_BFT_rate_ave,'-o','linewidth',2);hold on
% plot(SNR_range, post_BFT_rate_ave_div2,'-s','linewidth',2);hold on
% plot(SNR_range, post_BFT_rate_ave_div3,'-o','linewidth',2);hold on
plot(SNR_range, post_BFT_rate_ave_PAA,'--x','linewidth',2);hold on
% plot(SNR_range, ones(length(SNR_range),1)*mean(post_BFT_rate_cov),'-k','linewidth',2);hold on

set(gca,'fontsize',14)
xlabel('SNR [dB]')
ylabel('Spectral Efficiency [bps/Hz]')
% legend('TTD, K_B_T=1','TTD, K_B_T=2','TTD, K_B_T=3')
legend('TTD, 1 symbol','PAA, 32 symbols','PAA, 16 symbols','PAA, 8 symbols','PAA, 4 symbols')

% legend('TTD, K_B_T=1','TTD, K_B_T=2','TTD, K_B_T=3','PAA, K_B_T=32','PAA, K_B_T=16','PAA, K_B_T=8','PAA, K_B_T=4')
grid on

