clear;clc;
% rng(3);                                     %random seed
%-------------------------------------
% System Parameters
%-------------------------------------
Ts = 1/400e6;
fc = 28e9;                                  % carrier freq [Hz]
P = 2048;                                   % subcarrier number
sc_num = P;
Nr    = 16;                                 % Num of antenna in UE/Rx (total)
Nr_az = 16;                                  % Num of antenna in UE/Rx (azimuth)
Nr_el = 1;                                  % Num of antenna in UE/Rx (elevation)

Nt    = 1;                                 % Num of antenna in BS/Tx (total)                           
Nt_az = 1;                                  % Num of antenna in BS/Tx (azimuth) 
Nt_el = 1;                                 % Num of antenna in BS/Tx (elevation) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        QuaDRiGa Parameter and Setup
%     It uses QuaDRiGa Tutorial 4.3 as baseline
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Include QuaDRiGa source folder
addpath('C:\Users\Han\Documents\QuaDriGa_2017.08.01_v2.0.0-664\quadriga_src');

% Fonts setting
% set(0,'defaultTextFontSize', 18)                        % Default Font Size
% set(0,'defaultAxesFontSize', 18)                        % Default Font Size
% set(0,'defaultAxesFontName','Times')                    % Default Font Type
% set(0,'defaultTextFontName','Times')                    % Default Font Type
% set(0,'defaultFigurePaperPositionMode','auto')          % Default Plot position
% set(0,'DefaultFigurePaperType','<custom>')              % Default Paper Type
% set(0,'DefaultFigurePaperSize',[14.5 7.3])              % Default Paper Size

s = qd_simulation_parameters;                           % New simulation parameters
s.center_frequency = fc;                                % 28 GHz carrier frequency
s.sample_density = 4;                                   % 4 samples per half-wavelength
s.use_absolute_delays = 0;                              % Include delay of the LOS path
s.show_progress_bars = 0;                               % Disable progress bars

% Antenna array setting
l = qd_layout( s );                                     % New QuaDRiGa layout
l.tx_array = qd_arrayant('3gpp-mmw',Nt_el,Nt_az,fc,1,0,0.5,1,1);
l.rx_array = qd_arrayant('3gpp-mmw',Nr_el,Nr_az,fc,1,0,0.5,1,1);
% l.rx_array.rotate_pattern(180, 'z');
% l.rx_array.visualize(1);pause(1)
l.track = qd_track('linear',1,-pi);

% BS and UE location 
UE_dist = 20;                                            % BS/UE distance in x-axis
UE_height = rand*20;                                     % Height of UE (up to 50m)
BS_UE_pos_angle = (rand*90-45)/180*pi;
x_UE = UE_dist * cos(BS_UE_pos_angle);
%     y_UE = UE_dist * sin(BS_UE_pos_angle);
BS_height = 10;                                          % Height of BS
y_UE = rand * (2 * UE_dist * tan(45/180*pi))...
    - (UE_dist * tan(45/180*pi));                        % BS/UE dist in y axis(ramdom)
l.tx_position(:,1) = [0,0,BS_height].';                  % BS position
l.rx_position(:,1) = [UE_dist,y_UE,UE_height].';         % UE position
l.set_scenario('mmMAGIC_UMi_NLOS');                      % Set propagation scenario
%     l.visualize;                                           % Plot the layout

% Call builder to generate channel parameter
cb = l.init_builder;  


% Some customized setting (for debuging)
% cb.scenpar.PerClusterDS = 0;                            % Create new builder object
%     cb.scenpar.SF_sigma = 0;                                % 0 dB shadow fading
%     cb.scenpar.KF_mu = 0;                                   % 0 dB K-Factor
%     cb.scenpar.KF_sigma = 0;                                % No KF variation
%     cb.scenpar.SubpathMethod = 'mmMAGIC';
%     cb.plpar = [];                                          % Disable path loss model

% Call builder for small scale fading parameters
cb.gen_ssf_parameters;                                  % Generate large- and small-scale fading


for kk=1:length(cb.AoA)
    if cb.AoA(kk)<pi/2 && cb.AoA(kk)>-pi/2
        cb.pow(kk) = 0;
    end
end
% Drifting model for channel dynamics (off)
% s.use_spherical_waves = 1;                              % Enable drifting (=spherical waves)
% c = cb.get_channels;                                    % Generate channel coefficients
% c.individual_delays = 0;                                % Remove per-antenna delays

% Call builder to generate channel 
s.use_spherical_waves = 0;                              % Disable drifting
d = cb.get_channels;                                    % Generate channel coefficients

% Rearrange since QuaDRiGa uses different az/el order
% each column in MIMO is [el1; el2; etc]
% while ours is [az1, az2, etc].'
chan_coeff = zeros(Nr,Nt,cb.NumClusters,2);
for c_idx = 1:cb.NumClusters
    chan_MIMO_old = squeeze(d.coeff(:,:,c_idx,1));

    row_order = repmat(1:Nr_el:Nr,1,Nr_el) + kron((0:Nr_el-1),ones(1,Nr_az));
    col_order = repmat(1:Nt_el:Nt,1,Nt_el) + kron((0:Nt_el-1),ones(1,Nt_az));

    chan_MIMO_new1 = chan_MIMO_old(:,col_order);
    chan_MIMO_new2 = chan_MIMO_new1(row_order,:);

    chan_coeff(:,:,c_idx,1) = chan_MIMO_new2;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%        Convert into Channel Taps
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
H_QDG_f = zeros(Nr,Nt,P);
H_QDG_t = zeros(Nr,Nt,P);
frac_delay = 10e-9;

for pp=1:P
    for path_idx = 1:cb.NumClusters

    H_QDG_f(:,:,pp) = H_QDG_f(:,:,pp) +...
        exp(-1j*2*pi*(frac_delay + d.delay(path_idx,1))*(pp-1)/(Ts*P))...
        *squeeze(chan_coeff(:,:,path_idx,1));

    end
end

%%

    
for kk=1:length(cb.AoA)
    if cb.AoA(kk)>pi/2
        true_NLOS(kk) = cb.AoA(kk) - pi;
    elseif cb.AoA(kk)<-pi/2
        true_NLOS(kk) = cb.AoA(kk) + pi;
    else
        true_NLOS(kk) = cb.AoA(kk);
    end
end
figure
stem(true_NLOS/pi*180,cb.pow)
set(gca,'FontSize',14)
xlabel('angle [deg]')
ylabel('Pow')
% xlim([-90,90])
grid on

%%
theta_range = linspace(-pi/2,pi/2,500);
for theta_idx = 1:500
    arx = exp(1j*pi*(0:15).'*sin(theta_range(theta_idx)));
    gain(theta_idx) = abs(arx'*squeeze(H_QDG_f(:,:,1000))).^2;
end
figure
subplot(211)
plot(theta_range/pi*180,gain)
xlim([-90,90])
grid on
subplot(212)
if cb.AoA(1)>pi/2
    true_LOS = cb.AoA(1) - pi;
elseif cb.AoA(1)<-pi/2
    true_LOS = cb.AoA(1) + pi;
end
stem(true_LOS/pi*180,1)
xlim([-90,90])
grid on



