clear;

fc_num = 9;
fc_range_raw = linspace(27.8, 28.2, fc_num)*1e9-20e9;
M_selected_SC_num = 32;                     % num of selected SC (RB in fact)
M_selected_SC_gap = 2048/M_selected_SC_num;
M_loaded = (0: M_selected_SC_num-1) * M_selected_SC_gap + 1;
M_loaded_center = M_loaded + 6;

fc_range = fc_range_raw;%fc_range_raw(M_loaded_center);
f0 = 28e9;
c_speed = 3e8;
lambda0 = c_speed/f0;
d_scaling = 1;
d = lambda0/2*d_scaling;

dtau_az = 1/(400e6);% + d/c_speed*sin(0/180*pi);%*fc_num/(fc_num+1);
% dtau_az = d/c_speed*sin(45/180*pi);
dpsi_az = pi*sin(45/180*pi)
% dtau_az = 10/f0;
% dtau_el = 0.01765*16/c_speed;

N_az = 8;
N_el = 1;
theta_az_num = 500;
theta_az_range = linspace(-pi/2,pi/2,theta_az_num);
theta_el_num = 1;
theta_el_range = linspace(-pi/2,pi/2,theta_az_num);

error = (rand(N_az,1)*2-1) * 0e-12;
error_freq_depen = (rand(N_az,fc_num)*2-1) * 0e-12;

for ff=1:fc_num
    fc = fc_range(ff);
    lambda = c_speed/fc;
%     dphi_el = 2*pi*fc*dtau_el;
    
    array_win = kaiser(N_az,3)./norm(kaiser(N_az,3))*sqrt(N_az);
    phases_fromTTD = zeros(N_az,1);
    for kk=1:N_az
        
        phases_fromTTD(kk) = (kk-1) * 2 * pi * fc * dtau_az...
                            + 2 * pi * fc * error(kk);...
%                             + pi *(kk-1) * (0.5);%sin(0/180*pi);%...
%                             + 2 * pi * fc * error_freq_depen(kk,ff)...
%                             - 2 * pi * f0 * error(kk)...
%                             - 2 * pi * f0 * error_freq_depen(kk,5);
    end
    
    v_az_vec(:,ff) = exp(1j*phases_fromTTD);%.*array_win;
    v_PAA_az_vec = exp(1j*(0:N_az-1).'*dpsi_az);%.*array_win;
%     v_el_vec(:,ff) = exp(1j*(0:N_el-1).'*dphi_el);

    
    for tt=1:theta_az_num
        h_az(:,tt) = exp(-1j*2*pi*d/lambda*(0:N_az-1).'*...
                        sin(theta_az_range(tt)))/sqrt(N_az)^2;
%         h_el(:,tt) = exp(1j*2*pi*lambda0/2/lambda*(0:N_el-1).'...
%                         *sin(theta_el_range(tt)))/sqrt(N_el)^2;
        gain_az_mag(ff,tt) = v_az_vec(:,ff)'*h_az(:,tt);
        gain_az(ff,tt) = abs(gain_az_mag(ff,tt))^2;
        
        gain_PAA_az_mag(ff,tt) = h_az(:,tt)'*v_PAA_az_vec;
        gain_PAA_az(ff,tt) = abs(h_az(:,tt)'*v_PAA_az_vec)^2;
%         gain_el(ff,tt) = abs(h_el(:,tt)'*v_el_vec(:,ff))^2;
        
    end
end
%%
% fig = figure('units','inch','position',[0,0,16,4]);
% subplot(211)
% f_idx_temp = reshape((1:64).',16,4);
% f_idx = reshape(f_idx_temp(1:4,:),16,1);
f_idx = 1:9;

figure
polarplot(pi+theta_az_range, 10*log10(gain_az(f_idx,:)),'linewidth',3);
set(gca,'FontSize',18)
% title('TTD Pattern')

rlim([-20,0])
% thetalim([-90,90])
thetalim([90,270])
thetaticks(90:30:270)
thetaticklabels({'-90','-60','-30','0','30','60','90'})

% rlim([-10,0])
% thetalim([210,240])
% thetaticks(225)
% thetaticklabels({'45'})

grid on
legendtext = {};
for ff=1:length(f_idx)
    legendtext{ff} = ['f',num2str(ff),'=', num2str(fc_range(f_idx(ff))/1e9,'%.2f') 'GHz'];
end

% legendtext = {'a','b'};
legend(legendtext,'Location','eastoutside','FontSize',11)

%%
gain_envelop = max(gain_az,[],1);
[maxp,maxi] = min(gain_envelop);
figure
polarplot(pi+theta_az_range, 10*log10(gain_envelop),'linewidth',3);
hold on
polarplot(pi+theta_az_range(maxi), 10*log10(maxp),'r*','linewidth',4,'markersize',10);
hold on
polarplot(pi+theta_az_range, -4*ones(theta_az_num,1),'k--','linewidth',2);

set(gca,'FontSize',18)

rlim([-20,0])
thetalim([90,270])
% thetalim([-90,90])
thetaticks(90:30:270)
thetaticklabels({'-90','-60','-30','0','30','60','90'})
%% PAA pattern
% f_idx = 3;
% figure
% polarplot(pi+theta_az_range, 10*log10(gain_PAA_az(f_idx,:)),'linewidth',2);
% set(gca,'FontSize',18)
% title('PAA Pattern')
% rlim([-20,0])
% thetalim([90,270])
% thetaticks(90:30:270)
% thetaticklabels({'-90','-60','-30','0','30','60','90'})
% 
% % rlim([-10,0])
% % thetalim([210,240])
% % thetaticks(225)
% % thetaticklabels({'45'})
% 
% grid on
% legendtext = {};
% for ff=1:length(f_idx)
%     legendtext{ff} = [num2str(fc_range(f_idx(ff))/1e9,'%.2f') 'GHz'];
% end
% 
% legend(legendtext,'Location','eastoutside','FontSize',11)
% 

