clear;clc;
f=28.1e9;
fc=28e9;
dtau = 1/400e6;
N = 16;
theta = linspace(-pi/2,pi/2,500);
% gain1 = (1-exp(1j*pi*N*(2*f*dtau-sin(theta))))./(1-exp(1j*pi*(2*f*dtau-sin(theta))));
gain = sin(N*pi/2*(2*f*dtau-f/fc*sin(theta))).^2./sin(pi/2*(2*f*dtau-f/fc*sin(theta))).^2;
gain_PAA = sum(exp(1j*pi*sin(theta.')*(0:N-1)),2);
%%
figure
plot(theta,10*log10(abs(gain)),'o-');hold on
plot(theta,20*log10(abs(gain_PAA)),'--')
grid on
ylim([-20,30])
