clear;clc;
N_max_log = 11;
N_log = 4:1:N_max_log;
N = 2.^(N_log);
comp = zeros(length(N_log),N_max_log+1);
for n=1:length(N_log)
    comp(n,1) = N(n);
end

for stage_idx = 1:N_max_log
    for n = 1:length(N_log)
        if N(n)>=2^stage_idx
            comp(n,stage_idx+1) = comp(n,stage_idx) + 2^(stage_idx-1)*(stage_idx);
        else
            comp(n,stage_idx+1) = 0;
        end
    end
end

%%
figure('Renderer', 'painters', 'Position', [10 10 1200 600])
subplot(1,2,1)
loglog(N,comp(:,[1,4:N_max_log+1]),'o--','linewidth',2)
hold on
set(gca,'FontSize',14)
xticks(N)
xlim([min(N),max(N)])
ylim([1e1,1e5])
xlabel('Antenna Size')
ylabel('Complexity (mult. per sample)')
grid on
legend('1 stream','8 stream','16 stream','32 stream','64 stream','128 stream','256 stream','512 stream','1024 stream','2048 stream')

subplot(1,2,2)
loglog(N,max(comp,[],2),'linewidth',2)
hold on
[a,b] = max(comp,[],2);

loglog(N,diag(comp(1:end,b-3)),'linewidth',2)
hold on
loglog(N,comp(:,1),'linewidth',2)
hold on
loglog(N,max(comp,[],2),'o','linewidth',2)
set(gca,'FontSize',14)
xticks(N)
xlim([min(N),max(N)])
xlabel('Antenna Size')
ylabel('Complexity (mult. per sample)')
ylim([1e1,1e5])
grid on
legend('Prop. \beta = 1 (Full load)','Prop. \beta = 1/8 (Sweep Point)','Prop. \beta = 1/N (Single stream)','Conv. \forall \beta')