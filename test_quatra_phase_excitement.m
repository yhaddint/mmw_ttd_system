clear;clc
N= 16;
w0_phase = 4*((2*(0:N-1).' - (N-1))/(2*(N-1))).^2 * 0 * pi;
w1_phase = 4*((2*(0:N-1).' - (N-1))/(2*(N-1))).^2 * 1 * pi;
w2_phase = 4*((2*(0:N-1).' - (N-1))/(2*(N-1))).^2 * 2 * pi;

theta_range = linspace(-60,60,500);
for theta_idx = 1:length(theta_range)
    theta = theta_range(theta_idx)/180*pi;
    array_res = exp(1j*(0:N-1).'*pi * sin(theta))/sqrt(1);
    gain0(theta_idx) = abs(array_res'*exp(1j*w0_phase))^2;
    gain1(theta_idx) = abs(array_res'*exp(1j*w1_phase))^2;
    gain2(theta_idx) = abs(array_res'*exp(1j*w2_phase))^2;
end

%%
figure
plot(w0_phase,'-o','linewidth',2);hold on
plot(w1_phase,'-o','linewidth',2);hold on
plot(w2_phase,'-o','linewidth',2);hold on
xlim([1,16])
grid on
legend('\Phi_M = 0', '\Phi_M = \pi','\Phi_M = 2\pi')
xlabel('Antenna Element Index')
ylabel('Phase Excitation [rad]')
%%
figure
plot(theta_range, 10*log10(gain0),'linewidth',3);hold on
plot(theta_range, 10*log10(gain1),'linewidth',3);hold on
plot(theta_range, 10*log10(gain2),'linewidth',3)

xlabel('Angle [deg]')
ylabel('Gain [dB]')
grid on
legend('\Phi_M = 0', '\Phi_M = \pi','\Phi_M = 2\pi')
% ylim([0,40])
