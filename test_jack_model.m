clear;clc;
t=linspace(0,2e-3,1e3);
N = 1e3;
MCtimes = 1e2;
corr_fun = zeros(2e3-1,1);
for MCidx = 1:MCtimes
    alpha = rand(N,1)*2*pi;
    f=5e3;
    a = rand(N,1)*2*pi;
    ri = zeros(1e3,1);
    rq = zeros(1e3,1);

    for n=1:N
        ri = ri + cos(2*pi*f*cos(alpha(n))*t' + a(n))/sqrt(N);
    end
    r = ri;
    corr_fun = corr_fun + conv(r,flipud(r));
end
plot(corr_fun)
grid on