%%
clear;clc;
rng(0)
MCtimes = 1;

channel_model = 'sv';                    % 'quadriga' or 'sv'
c_speed = 3e8;                           % speed of light
BW = 5e9;                                % Bandwidth in [Hz]
Ts = BW;                                 % Sampling duration [second]
fc = 140e9;                              % Carrier freq [Hz]
f0 = fc;                                 % Carrier freq [Hz]
lambda0 = c_speed/f0;                    % wavelength of carrier

% T/Rx array related parameters
Nr    = 64;                             % Num of antenna in UE/Rx (total)
Nr_az = 64;                             % Num of antenna in UE/Rx (azimuth)
Nr_el = 1;                               % Num of antenna in UE/Rx (elevation)
Nt    = 1;                               % Num of antenna in BS/Tx (total)                           
Nt_az = 1;                               % Num of antenna in BS/Tx (azimuth) 
Nt_el = 1;                               % Num of antenna in BS/Tx (elevation) 

% OFDMA and TTD related parameters
P = 256;                                % subcarrier number
sc_num = P;                              % subcarrier number
f_range = linspace(fc-BW/2, fc+BW/2, sc_num);
dtau_az = 1/BW;                          % TTD delay difference

% MIMO related parameters
layer_num = 8;                          % number of MIMO layer 

% M_selected_SC_num = 32;                  % num of selected SC (RB in fact)
% M_selected_SC_gap = sc_num/M_selected_SC_num;
% M_loaded = (0: M_selected_SC_num-1) * M_selected_SC_gap + 1;
% M_loaded_center = M_loaded + RB_len/2;
% M_loaded_all = reshape(repmat((M_loaded-1),RB_len,1) + repmat((1:RB_len)',1,M_selected_SC_num),M_selected_SC_num*RB_len,1);

% Scheduling related parameters
RB_len = 1;                             % length of resource block
RB_num = floor(sc_num/RB_len);
UE_num = 200;                           % Total number of UE simulated (not guaranteed served)
max_UE_range = [200];
Gain_TH = 10*log10(Nr) - 4;              % Good gain indicator (allow scheduling)

% Channel related parameters
AOAspread_az = 0/180*pi;                 % Intra-cluster AoA spread square 
cluster_num = 1;                         % number of channel cluster (SV model)
path_num = 1;                           % number of rays within cluster (SV model)

% Evaluation related parameters
SNR_num = 11;                            % SNR sweep number
SNR_range = linspace(-30,0,SNR_num);     % SNR evaluation range

% Initialize error symbol counter
symb_cnt = 0;
error_cnt_prop = zeros(SNR_num,1);
error_cnt_ideal = zeros(SNR_num,1);

% Some pre-computing
array_win = kaiser(Nr_az,3)./norm(kaiser(Nr_az,3))*sqrt(Nr_az);

for MCidx = 1:MCtimes
    clc;fprintf('Iteration %d',MCidx)
    
    % BS (TTD) sounding uses fixed dispersive rainbow beams
    dphi_az = 2*pi*f_range*dtau_az;
    v_az_vec = exp(1j*(0:Nr_az-1).'*dphi_az)/sqrt(Nr_az);
    
    H_chan_WB_norm = zeros(Nr,P,UE_num);
    for UE_idx = 1:UE_num
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        %        In-House S-V Channel
        %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        theta0_az(MCidx, UE_idx) = rand * 2 * pi/2 - pi/2;
%         gain_az_mag = zeros(sc_num,1);
%         gain_az_mag = zeros(M_selected_SC_num,1);
        c_tau = 0.01; % 23 % intra-cluster delay spread with unit [ns]
        r_tau = 2.03; % parameter in mmMAGIC
        relative_delay_prime = -r_tau*c_tau*log(rand(path_num,1));
        relative_delay = sort(relative_delay_prime) - min(relative_delay_prime);
        delay_pow_scling = exp(-relative_delay*(r_tau-1)./(r_tau*c_tau));
        ray_delay = (relative_delay+10)*1e-9;
        theta_az = theta0_az(MCidx, UE_idx)...
                            + laprnd(path_num, 1, 0, AOAspread_az);
        rayAOA_az = theta_az;
        rayAOA_el = zeros(path_num,1);
        rayAOD_az = zeros(path_num,1);
        rayAOD_el = zeros(path_num,1);
        g_ray = exp(1j*rand(path_num,1)*2*pi) .* delay_pow_scling;

        h_az = zeros(Nr_az, 1);
        [H_chan_WB,~] = get_H_WB_3D(g_ray.',...
                            ray_delay.',...
                            rayAOA_az.',...
                            rayAOA_el.',...
                            rayAOD_az.',...
                            rayAOD_el.',...
                            cluster_num,...        % cluster number
                            path_num,...           % ray number
                            Nt_az, 1,...
                            Nr_az, 1,...
                            Ts,sc_num);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %
        %          Channel selection and normalization
        %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%         H_chan_WB_sel = H_chan_WB./norm(squeeze(H_chan_WB))*sqrt(P*Nr_az);
        H_chan_WB_sel = H_chan_WB * sqrt(Nr_az);

        H_chan_WB_ordered = H_chan_WB_sel(:,:,[sc_num/2+1:end, 1:sc_num/2]);
        H_chan_WB_norm(:,:,UE_idx) = v_az_vec.*squeeze(H_chan_WB_ordered);
    end
    UE_range = rand(UE_num,1)*70+30; % For illustration only (assuming UL power CTRL)
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %          TDD Multi-User Multi-Layer Sounding Phase
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % for loop for MIMO layer 
    gain_az_mag = zeros(P, UE_num, layer_num); % contains RSSI of each Subcarrier-User-MIMO_layer pair
    
    DFT_rotation2 = diag(array_win) * exp(1j*(0:Nr_az-1).'*2*pi/layer_num*(0:layer_num-1));
    for sc_idx = 1:P
        gain_az_mag(sc_idx,:,:) = abs((DFT_rotation2'*squeeze(H_chan_WB_norm(:,sc_idx,:))).');
    end

        
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    %          OFDMA Scheduler based on RSS Report
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for max_UE_idx = 1:length(max_UE_range)
        unserved_UE = 1:max_UE_range(max_UE_idx);
        sc_assign = zeros(P,layer_num);
        sc_assign_sparse = zeros(P,UE_num);
        for RB_idx=1:RB_num
            for layer_idx = 1:layer_num
                sc_mask = zeros(P,layer_num);
            
                sc_idx = (RB_idx-1)*RB_len+1:RB_idx*RB_len;
                Gain_RB = 20*log10(squeeze(gain_az_mag(sc_idx,:,layer_idx)));
                Gain_RB_min = min(Gain_RB,[],1);
                UE_good_gain = find(Gain_RB_min>Gain_TH);
                UE_to_serve = intersect(UE_good_gain, unserved_UE);
                if isempty(UE_to_serve) ~= 1
                    % randomly pick a UE from feasible set
                    UE_sel = UE_to_serve(randi(length(UE_to_serve)));

                    % setup picked UE in results table
                    sc_assign(sc_idx, layer_idx) = UE_sel;
                    sc_mask(sc_idx, layer_idx) = layer_idx;
                    sc_assign_sparse(sc_idx, UE_sel) = 1;

                    % update unserved UE array
                    unserved_UE = unserved_UE(unserved_UE~=UE_sel);
                end
            end
            
        end
        UE_serve_rate(MCidx,max_UE_idx) = length(find(sc_assign>0))/RB_len;
    end

    
    %
end
%%
sc_idx = 1;
layer_range = 1:8;
figure
for ss=1:length(layer_range)
    ue_served = sc_assign(sc_idx, ss);
    if ue_served>0
        polarplot(theta0_az(MCidx, ue_served),UE_range(ue_served),'o','linewidth',3)
        hold on
        if ss==length(layer_range)
            polarplot(theta0_az(MCidx, ue_served),UE_range(ue_served),'ko','linewidth',3)
        end
    else
        polarplot(0,0,'o','linewidth',1);
        hold on
    end
end
polarplot(theta0_az(MCidx, :),UE_range,'x','linewidth',0.5,'Color',[57 57 57]/255)
hold on

grid on
rlim([0,110])
thetalim([-90,90])

gain_angle = zeros(500,length(layer_range));
for layer_idx = 1:length(layer_range)
    layer_sel_idx = layer_range(layer_idx);
    DFT_rotation = array_win .* exp(1j*(0:Nr_az-1).'*2*pi/layer_num*(layer_sel_idx-1));
    gain_angle(:,layer_idx) = abs(exp(-1j*pi*sin(linspace(-pi/2,pi/2,500)')*(0:Nr_az-1))*(DFT_rotation.*v_az_vec(:,sc_idx)));
end
figure
polarplot(linspace(-pi/2,pi/2,500),20*log10(gain_angle(:,1:7)),'linewidth',2)
hold on
polarplot(linspace(-pi/2,pi/2,500),20*log10(gain_angle(:,8)),'k','linewidth',2)
rlim([-15,20])
set(gca,'FontSize',14)
thetalim([-90,90])
lgdtext = {}
for ii=1:8
    lgdtext{ii} = ['Layer k=',num2str(ii-1)];
end
legend(lgdtext)
%%
lgdtext = {}
gain_angle = zeros(500,sc_num/16);
for sc_idx = 1:sc_num/16
    sc_idx_true = (sc_idx-1)*16+1;
    layer_idx = 1;
    layer_sel_idx = 1;
    DFT_rotation = array_win .* exp(1j*(0:Nr_az-1).'*2*pi/layer_num*(layer_sel_idx-1));
    gain_angle(:,sc_idx) = abs(exp(-1j*pi*sin(linspace(-pi/2,pi/2,500)')*(0:Nr_az-1))*(DFT_rotation.*v_az_vec(:,sc_idx_true)));
    lgdtext{sc_idx} = ['RB b=',num2str(sc_idx_true-1)];
end
figure
polarplot(linspace(-pi/2,pi/2,500),20*log10(gain_angle(:,1:7)),'linewidth',2)
hold on
polarplot(linspace(-pi/2,pi/2,500),20*log10(gain_angle(:,8)),'k','linewidth',2)
hold on
polarplot(linspace(-pi/2,pi/2,500),20*log10(gain_angle(:,9:15)),'-','linewidth',2)
hold on
polarplot(linspace(-pi/2,pi/2,500),20*log10(gain_angle(:,16)),'k-','linewidth',2)
hold on
rlim([-15,25])
set(gca,'FontSize',14)
thetalim([-90,90])
legend(lgdtext)




