clear;clc
N=64;
steer_angle = 20/180*pi;
w1_phase = 4*((2*(1:N).' - (N-1))/(2*(N-1))).^2 * 1 * pi...
    - (0:N-1).'*pi*sin(steer_angle);
w2_phase = -(0:N-1).'*pi*sin(steer_angle);
w3_phase = -(0:N/4-1).'*pi*sin(steer_angle);
theta_range = linspace(-60,60,500);
for theta_idx = 1:length(theta_range)
    theta = theta_range(theta_idx)/180*pi;
    array_res = exp(1j*(0:N-1).'*pi * sin(theta))/sqrt(1);
    array_res2 = exp(1j*(0:N/4-1).'*pi * sin(theta))/sqrt(1);
    array_res4 = exp(1j*(0:N/4-1).'*pi * sin(theta))/sqrt(1/4);
    gain1(theta_idx) = abs(array_res'*exp(1j*w1_phase))^2;
    gain2(theta_idx) = abs(array_res'*exp(1j*w2_phase))^2;
    gain3(theta_idx) = abs(array_res2'*exp(1j*w3_phase))^2;
    gain4(theta_idx) = abs(array_res4'*exp(1j*w3_phase))^2;
end
%%
figure
plot(theta_range, 10*log10(gain2),'k','linewidth',1);hold on
plot(theta_range, 10*log10(gain3),'linewidth',3);hold on
plot(theta_range, 10*log10(gain4),'linewidth',3)
plot(theta_range, 10*log10(gain1),'linewidth',3);hold on

xlabel('Angle [deg]')
ylabel('EIRP [dBm]')
grid on
legend('DFT Pencil Beam', 'Phase-only Wide Beam (method1)',...
    'Phase-only Wide Beam (method1, extra power)', 'Phase-only Wide Beam (method2)')
ylim([0,40])