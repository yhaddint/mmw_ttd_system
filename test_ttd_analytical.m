clear;clc;
M_tot_range = [128];
OS = 1;
freq_channel_shifted = zeros(1024,4);
for M_idx = 1:length(M_tot_range)
    M_tot = M_tot_range(M_idx);
    N = 16;
    theta1 = 0/180*pi;
%     theta2 = -20/180*pi;
    w1_vec = exp(1j*pi*(0:N-1).'*sin(theta1));
%     w2_vec = exp(1j*pi*(0:N-1).'*sin(theta2));
%     w1_vec_OS = zeros(N,1);
%     w1_vec_OS = w1_vec(1:2:end);
%     for ii=1:N
%         if mod(ii,OS)==1
%             w1_vec_OS(ii) = w1_vec(ii);
%         else
%             w1_vec_OS(ii) = w1_vec(ii-1);
%         end
%     end
            
    freq_channel = fft([w1_vec ;zeros(M_tot-N,1)]);
    freq_channel2 = fft([0;w1_vec ;zeros(M_tot-N-1,1)]);
    freq_channel_shifted(1:M_tot,M_idx) = abs([freq_channel(M_tot/2+1:end);freq_channel(1:M_tot/2)]).^2;

    freq_channel_shifted2(1:M_tot,M_idx) = abs([freq_channel2(M_tot/2+1:end);freq_channel2(1:M_tot/2)]).^2;
end
%%
figure
for M_idx = 1:length(M_tot_range)
    M_tot = M_tot_range(M_idx);
    xdata = linspace(-200,200,M_tot);
    subplot(1,1,M_idx)
    plot(xdata, freq_channel_shifted(1:M_tot,M_idx),'-o','linewidth',2,'markersize',2)
    hold on
    plot(xdata, freq_channel_shifted2(1:M_tot,M_idx),'k-o','linewidth',2,'markersize',2)

    hold on
    grid on
    xlabel('Freq. of Subcarrier')
    ylabel('Freq Chan. Mag')
    set(gca,'FontSize',12)
    titletext = [num2str(M_tot_range(M_idx)), ' Subcarrier']
    title(titletext)
end


