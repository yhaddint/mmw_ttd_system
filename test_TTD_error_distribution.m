clear;clc;
MCtimes = 1e3;
for MCidx = 1:MCtimes
    results(:,MCidx) = dftmtx(16)*randn(16,1);
end
results_new = reshape(results, MCtimes*16,1);
figure
subplot(211)
hist(real(results_new))
subplot(212)
hist(imag(results_new))