clear;clc;
tau1=32e-9;
tau2=32e-9;
Ts=5e-9;
sc_num = 128;
for pp=1:sc_num
    hf(pp) = exp(-1j*2*pi*(pp-1)*tau1/sc_num/Ts);...
%            + 0.5 * exp(-1j*2*pi*(pp-1)*tau2/sc_num/Ts);
end

% From sinc function
xdata = linspace(0,sc_num-1,(sc_num-1)*10+1);
xdata_cont = xdata;

% ydata = 0.5*(1-cos(2*pi*(xdata-tau1)/(1/128)/length(xdata)+1));% + 0.5 * sinc(xdata-tau2/Ts);

ydata = sinc(xdata-tau1/Ts);% + 0.5 * sinc(xdata-tau2/Ts);

time_sample = ydata(1:10:end);

%% plot (time domain)
figure
plot((0:sc_num-1)*Ts/1e-9,abs(ifft(hf)),'o','linewidth',2,'markersize',4)
hold on
plot(xdata*Ts/1e-9,ydata,'linewidth',2)
hold on
plot(xdata(1:10:end)*Ts/1e-9,time_sample,'x','linewidth',2)
grid on
xlim([0,200])
xlabel('Time [ns]')

%% plot (freq domain)
% figure;
% plot(abs([hf(end/2+1:end),hf(1:end/2)]),'-o')
% hold on
% freq_chan_fft = fft(time_sample);
% plot(abs([freq_chan_fft(end/2+1:end),freq_chan_fft(1:end/2)]),'-x')
% xlabel('Feeq.')
% legend('Direct from \tau','FFT of Time taps')
% 


