clear;clc;
N_cand = [32,64,128,256,512,1024];
beta_inv = [8, 16];
M = 2048;
W = 10;
beta = (1./beta_inv)';

for ant_idx = 1:length(N_cand)
    for mimo_idx = 1:length(beta)
        N = N_cand(ant_idx);
        K = beta(mimo_idx) * N;
        
        MIMO_conv(mimo_idx,ant_idx) = N*log2(M) + K*N;
        % MIMO_BeamFFT = N.*log2(N) + W*beta*N;
        MIMO_NFFT(mimo_idx,ant_idx) = N*log2(M) + N*log2(N);
        MIMO_KFFT(mimo_idx,ant_idx) = N*log2(M) + N + K*log2(K);
%         MIMO_proposed(mimo_idx,ant_idx) = K*log2(M) + K*log2(K) + K*K;
        MIMO_proposed(mimo_idx,ant_idx) = K*log2(M) + K*log2(K);
    end
end
%%
figure
loglog(N_cand,MIMO_conv,'linewidth',2)
hold on
loglog(N_cand,MIMO_NFFT(1,:),'linewidth',2)
hold on
loglog(N_cand,MIMO_KFFT,'linewidth',2)
hold on
% loglog(N,MIMO_BeamFFT,'linewidth',2)
% hold on
loglog(N_cand,MIMO_proposed,'linewidth',2)
set(gca,'FontSize',14)
xlabel('Antenna Number')
ylabel('Complex Multiplication')
grid on
ylim([1e1,1e7])
xticks([32,64,128,256,512,1024])
legend('Conv MIMO (\beta = 1/8)',...
       'Conv MIMO (\beta = 1/16)',...
       'N-FFT MIMO (any \beta)',...
       'K-FFT MIMO (\beta = 1/8)',...
       'K-FFT MIMO (\beta = 1/16)',...
       'Proposed (\beta = 1/8)',...
       'Proposed (\beta = 1/16)')
%        'Beam Space MIMO (\beta = 1/8)',...
%        'Beam Space MIMO (\beta = 1/16)',...
