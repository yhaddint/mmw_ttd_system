clear;clc;
N = 32;
% symb = randn(N,1) + 1j * randn(N,1);
symb = ones(N,1);
F = dftmtx(N)';
F_DS = dftmtx(N/8);
TRx_mtx = F_DS * F(1:8:end,:);
symb_rx = TRx_mtx * symb;
%%
[symb(1:4),symb_rx(1:4)]