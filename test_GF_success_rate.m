clear;clc;
path_num = 3;                          % number of multipath b/w each UE to BS
UE_active_rate = 1;                    % probability of a UE in pool is active
UE_num_range = [100];                  % total number of UE
UE_density_num = length(UE_num_range);
Nbs = 128;                             % number of antenna in BS
K = 32;                                % number of MIMO layer
Nue = 16;                              % number of atenna in UE
RB = 256;                              % number of resource block
HARQ_max_num = 4;                      % number of maximu  transmission
RB_map_raw = linspace(-1,1,RB)';
RB_LUT = zeros(RB,K);
for kk=1:K
    shift_value = (kk-1) * (RB/K);
    RB_LUT(:,kk) = circshift(RB_map_raw,shift_value);
end
MCtimes = 1e5;
latency = ones(MCtimes,1)*50;
for MCidx = 1:MCtimes
    if mod(MCidx,1e3)
        clc;fprintf('Current Progress %.2f', MCidx/MCtimes);
    end
    % Typical UE; UE of interest (UOI)
    AoA_UOI = rand(1, path_num) * 2 - 1;            % AoA realizations
    AoD_UOI = rand(1, path_num) * 2 - 1;            % AoD realizations
    path_gain_UOI = exprnd(0.5, 1, path_num);       % power of path gain
    RB_inband_start_UOI = randi(RB - RB/K, 1);      % Narrowband nature of UE
    RB_inband_UOI = RB_inband_start_UOI + (1:RB/K); % Narrowband nature of UE
    for path_idx = 1:path_num
        [RB_UOI(path_idx), layer_UOI(path_idx)] = findRB(AoA_UOI(path_idx),RB_LUT, RB_inband_UOI, K, RB);
    end
    
    % pre-generate all UEs channel parameter
    UE_num_max = UE_num_range(end);
    AoA = rand(UE_num_max, path_num) * 2 - 1;       % AoA realizations
    AoD = rand(UE_num_max, path_num) * 2 - 1;       % AoD realizations
    path_gain = exprnd(0.5, UE_num_max, path_num);      % power of path gain
    RB_inband = randi(RB - RB/K, UE_num_max);           % Narrowband nature of UE
    for uu=1:UE_num_max
        for path_idx = 1:path_num
            RB_inband_now = RB_inband(uu) + (1:RB/K);
            AoA_now = AoA(uu, path_idx);
            [RB_all_UE(uu,path_num), layer_all_UE(uu,path_num)] = findRB(AoA_now,RB_LUT, RB_inband_now, K, RB);
        end
    end
    
    % predefine path selection scheme in all ARQ
    HAQR_idx = 0;
    continue_flag = 1;
    while HAQR_idx < 3 & continue_flag==1
        for UE_idx = 1:UE_num_max 
            path_sel(:,UE_idx) = randperm(path_num).';
        end
    
    
    % For loop for different UE density
        UE_num = UE_num_range(1);
        UE_active = randperm(UE_num_max);
        interference = ones(path_num,1);
        sig_pow = zeros(path_num,1);
        
        for path_idx = 1:path_num

            % signal power
            sig_pow(path_idx) = path_gain_UOI(path_idx)*Nbs*Nue;
            
            % Other UE behavior
            for uu=1:UE_num
                UE_now = UE_active(uu);
                path_now = path_sel(path_idx, uu);
                RB_inband_now = RB_inband(UE_now) + (1:RB/K);
                
                AoA_now = AoA(UE_now, path_now);
                AoD_now = AoD(UE_now, path_now);
                RB_now = RB_all_UE(UE_now, path_now);
                layer_now = layer_all_UE(UE_now, path_now);
                
                % collision (interference has full array gain)
                if RB_now == RB_UOI(path_idx) & layer_now == layer_UOI(path_idx)
                    interference(path_idx) = interference(path_idx) + path_gain(UE_now,path_idx)*Nbs*Nue;
                end
                
                % interference 
                if RB_now == RB_UOI(path_idx) & layer_now ~= layer_UOI(path_idx)
                    % Through the path UE selected
                    AoA_diff = AoA_now - RB_LUT(RB_UOI(path_idx), layer_UOI(path_idx));
                    UE_gain = Nue;
                    BS_gain = abs(sin(Nbs*pi/2*AoA_diff)/sin(pi/2*AoA_diff));
                    interference(path_idx) = interference(path_idx) + path_gain(UE_now,path_now)*UE_gain*BS_gain;
                    
                    % Through other paths (no UE gain)
                    AoD_remaining = AoD(UE_now, [1:path_now-1,path_now+1:path_num]);
                    AoA_remaining = AoA(UE_now, [1:path_now-1,path_now+1:path_num]);
                    gain_remaining = path_gain(UE_now, [1:path_now-1,path_now+1:path_num]);
                    for other_path_idx = 1:path_num-1
                        AoA_diff = AoA_now - AoA_remaining(other_path_idx);
                        AoD_diff = AoD_now - AoD_remaining(other_path_idx);
                        BS_gain = abs(sin(Nbs*pi/2*AoA_diff)/sin(pi/2*AoA_diff));
                        UE_gain = abs(sin(Nue*pi/2*AoD_diff)/sin(pi/2*AoD_diff));
                        interference(path_idx) = interference(path_idx) + gain_remaining(other_path_idx)*UE_gain*BS_gain;
                    end
                    
                end
            end
                        
        end
        
        % SINR computation
        SINR = sig_pow./interference;
        [aa,bb] = max(SINR);
        if aa>10
            latency(MCidx) =  (HAQR_idx) * (path_num+3) + (bb+1) + rand;
            continue_flag = 0;
        else
            HAQR_idx = HAQR_idx + 1;
            continue_flag = 1;
        end
    end
end
%%
latency_sort = sort(latency,'descend');
max_ytick = floor(log2(MCtimes));
ydata = 2.^[0:max_ytick];
xdata = latency_sort(ydata);
figure
semilogy(xdata,ydata/MCtimes,'-o','linewidth',2)
grid on
xlim([0,30])
xlabel('Latency [TTI]')
ylabel('CCDF')
