clear;clc;
M = 1e2; % Number of IoT devices
lambda = 60; % traffic arrival rate (i.i.d. 60fps for each IoT)
TTI = 0.2e-3;
Tmax = 10e-3;
N = Tmax/TTI;
rho = M*lambda*TTI;
b = zeros(N,1); 
b(1) = 1;
for n=1:N-1
    for kk=1:n+1
        k = kk-1;
        b(n+1) = b(n+1) + (-1)^k/factorial(k)*(n-k)^k*exp((n-k)*rho)*rho^k;
    end
end

P = zeros(N,1)
for n=1:N+1
    if n==1
        P(n) = 1/(1+rho*b(N));
    elseif n==N+1
        P(n) = 1-b(N)/(1+rho*b(N));
    else
        P(n) = (b(n)-b(n-1))/(1+rho*b(N));
    end
    
end
%%
figure
plot((0:N)*TTI/1e-3, P,'-o','linewidth',2)
grid on
xlabel('Delay [ms]')
ylabel('Probability')