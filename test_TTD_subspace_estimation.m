clear;clc
% rng(1)
fc_num = 16;
fc_range = linspace(27.8, 28.2, fc_num)*1e9;

f0 = 28e9;
c_speed = 3e8;
lambda0 = c_speed/f0;
d = lambda0/2;

dtau_az = 1/(400e6);

N_az = 32;
theta_az_num = 500;
theta_az_range = linspace(-pi/2,pi/2,theta_az_num);


lambda = c_speed./fc_range;
% phases_fromTTD = zeros(N_az,1);

% phases_fromTTD = (0:N_az-1).' * 2 * pi * fc * dtau_az;

ray_num = 100;
angular_spread = 30; % In degree
% theta = (rand(ray_num,1)*15-7.5)';
theta = linspace(-angular_spread,angular_spread,ray_num);
% MCtimes = 1e3;


% for MCidx = 1:MCtimes
% 
%     alpha = exp(1j*rand(ray_num,1)*2*pi);
% %     alpha = randn(ray_num,1) + 1j*randn(ray_num,1);
%     h_az(:,MCidx) = sum(exp(-1j*pi*(0:N_az-1).'*...
%                 sind(theta)) * diag(alpha),2);
% 
% end

%% True COV generation
COV_true = zeros(N_az,N_az);
% alpha = ones(ray_num,1);
alpha = rand(ray_num,1);
for ray_idx = 1:ray_num
    arx = exp(-1j*pi*(0:N_az-1).'*sind(theta(ray_idx)));
    COV_true = COV_true + alpha(ray_idx)*arx*arx';
end
[V,D] = eig(COV_true);
[~,idx_temp] = max(diag(abs(D)));
best_gain = V(:,idx_temp)'*COV_true*V(:,idx_temp);

steer_angle_range = linspace(-30,30,101);

for steer_idx = 1:length(steer_angle_range)
    w_vec = exp(-1j*pi*(0:N_az-1).'*sind(steer_angle_range(steer_idx)))/sqrt(N_az);
    gain(steer_idx) = w_vec'*COV_true*w_vec;
end
figure
plot(steer_angle_range,10*log10(abs(gain)),'linewidth',2)
hold on
plot(steer_angle_range,ones(length(steer_angle_range),1)*10*log10(abs(best_gain)),'--','linewidth',2)
ylabel('Receiver Gain [dB]')
grid on
xlabel('Steering Angle')
% ylim([10,22])
%%
COV = zeros(N_az,N_az);
for MCidx = 1:500
    COV = COV + h_az(:, MCidx)*h_az(:, MCidx)';
end
[V1,D1] = eig(COV);
[~,test1_idx] = max(abs(diag(D1)));

COV2 = zeros(N_az,N_az);
for MCidx = 501:1e3
    COV2 = COV2 + h_az(:, MCidx)*h_az(:, MCidx)';
end
[V2,D2] = eig(COV2);
[~,test2_idx] = max(abs(diag(D2)));

%%

figure
[b,a] = ecdf(20*log10(abs(V1(:,test1_idx)'*h_az)));
plot(a,b,'linewidth',2)
hold on
[b,a] = ecdf(20*log10(abs(V_best_norm'*h_az)));
plot(a,b,'linewidth',2)
hold on
[b,a] = ecdf(20*log10(abs((ones(N_az,1)/sqrt(N_az))'*h_az)));
plot(a,b,'linewidth',2)
hold on
set(gca,'FontSize',14)
grid on
legend('Subspace 1','MySubspace 1','DFT beam')
xlabel('Post Training Gain [dB]')
ylabel('CDF')
xlim([20,35])
%%
% v_az_vec = exp(1j*phases_fromTTD);
% 
% for tt=1:theta_az_num
%     h_az(:,tt) = exp(-1j*2*pi*d/lambda*(0:N_az-1).'*...
%                     sin(theta_az_range(tt)))/sqrt(N_az)^2;
% 
%     gain_az_mag(ff,tt) = v_az_vec(:,ff)'*h_az(:,tt);
%     gain_az(ff,tt) = abs(gain_az_mag(ff,tt))^2;
% end
%         
