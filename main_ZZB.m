clear;clc;
M = 64;                                          % used number of subcarrier (no divisity)
fc = 60e9;                                       % center frequency
BW = 2e9;                                        % bandwidth
Dict_step = 1024;                                % angle candidates in estimator
best_MSE = sqrt((180/Dict_step)^2/12);           % MSE due to finite dictionary grid in [deg]
dtau = 1/2e9;                                    % delta tau in [s]
Nr = 16;                                         % number of antenna
freq = linspace(fc - BW/2, fc + BW/2, M)';       % frequency in RF
TTD_error_num = 20;                              % TTD error sweep size
TTD_error_range = linspace(0,10,TTD_error_num);  % TTD error sweep in [ps]
MCtimes = 5e3;                                   % Monte Carlo runs

%% ZZB with sweeping additive mismatch error variance (test shape)
% for sigma_idx = 1:sigma_num
%     sigma = sigma_range(sigma_idx)*sqrt(sum(s1.^2));
%     for x_idx=1:length(x_range)
%         x = x_range(x_idx);
%         s2 = sin(Nr*pi/2*(2*freq*dtau+sin(x)))./sin(pi/2*(2*freq*dtau+sin(x)));
%         prob_int(sigma_idx) = prob_int(sigma_idx)...
%             +(1-x/pi) * qfunc(norm(s2-s1,2)/(sigma))*x*dx;
%     end
% end
% 
% %
% final_error = prob_int/pi*180 + ones(sigma_num,1) * best_MSE;
% figure
% semilogy(sigma_range/(2*pi)/fc/1e-12, final_error,'.-','linewidth',2)
% set(gca,'FontSize',14)
% hold on
% grid on
% xlabel('RMS TTD Error [ps]')
% ylabel('MSE [deg]')

%% Emprical evaluation of variance of mismatch error (as additive noise)

% Initialize empty matrix
error_dict_MC = zeros(TTD_error_num, MCtimes);

% received signal when AoA is 0
% w_true = exp(-1j*2*pi*freq*((0:Nr-1) * dtau));
% arx = exp(-1j*(0:Nr-1).'*pi*sin(0.2));
% s1 = w_true * arx;
s1 = exp(-1j*(Nr-1)*pi/2*(2*freq*dtau+sin(0)))...
    .*sin(Nr*pi/2*(2 * freq * dtau+sin(0)))...
    ./sin(pi/2*(2*freq*dtau+sin(0))); 

% figure
% plot(real(s1))
% hold on
% plot(real(s1_theo))

for MCidx = 1:MCtimes
    for TTD_error_idx = 1:TTD_error_num
        
        % When the true signal is |s1|, received signal due to dictionary
        % mismatch is |s1_tilde|; Emprically evaluating additive error
        TTD_error = randn(Nr,1) * TTD_error_range(TTD_error_idx) * 1e-12;
        w_tilde = exp(-1j*2*pi*freq*((0:Nr-1) * dtau + TTD_error'));
        arx = ones(Nr,1);
%         s1_tilde = abs(w_tilde * arx);
        s1_tilde = w_tilde * arx;
        
        % Huristic way of modeling mismatch as additive noise
        % Somehow works great in Monte Carlo simulation test (last cell)
%         error_dict_MC(TTD_error_idx, MCidx) = max(abs(s1_tilde - abs(s1)).^2);       
        error_dict_MC(TTD_error_idx, MCidx) = max(abs(s1_tilde - s1).^2);       

    end
end

% Emprically evalute std. var. of additive error
sigma_range = sqrt(mean(error_dict_MC,2));

% Theoretical dictionary error
error_phi = TTD_error_range*1e-12*fc*2*pi;

% Second order approximation: exp(1j*phi) = 1j*phi - phi.^2
% sigma_theo = sqrt(Nr)*abs(1j*error_phi - error_phi.^2);
sigma_theo = sqrt(Nr)*abs(1j*error_phi*2);


% comparison
figure
plot(TTD_error_range, sigma_range,'linewidth',2)
hold on
plot(TTD_error_range, sigma_theo,'linewidth',2)
grid on
xlabel('RMS TTD Error [ps]')
ylabel('Std Var')
legend('Emprical Dict. Error','Theo Dict Error')

%% ZZB versus emprical additive mismatch error

% Some pre-computation for numerical integral
x_range = linspace(0, pi/2, 1e3);
dx = x_range(2) - x_range(1);

% Initialize empty matrix
ZZB = zeros(TTD_error_num,1);

% Sweep TTD error
for sigma_idx = 1:TTD_error_num
    sigma = sigma_theo(sigma_idx);

    % numerical integral of ZZB equation over x
    for x_idx=1:length(x_range)
        x = x_range(x_idx);
%         s2 = sin(Nr*pi/2*(2*freq*dtau+sin(x)))./sin(pi/2*(2*freq*dtau+sin(x)));
        s2 = exp(-1j*(Nr-1)*pi/2*(2*freq*dtau+sin(x)))...
                .*sin(Nr*pi/2*(2 * freq * dtau+sin(x)))...
                ./sin(pi/2*(2*freq*dtau+sin(x))); 
        
        % Detection error rate of nearest neighbor
%         fa_rate(x_idx) = qfunc(norm(abs(s2)-abs(s1),2)/(2*sigma));
        fa_rate(x_idx) = qfunc(norm(s2 - s1, 2)/(2*sigma));
        
        % Main equation in ZZB to convert detection error to MSE
        ZZB(sigma_idx) = ZZB(sigma_idx) + (1/(pi/2)) * fa_rate(x_idx)*x*dx;
    end
end

% Convert [rad] to [deg] and add constant to model MSE due to finite grid 
final_MSE_bound = ZZB / pi * 180 + ones(TTD_error_num,1) * best_MSE;

%% Monte Carlo evaluation of LoS AoA estimation with
% 1) True dictionary mismatch error
% 2) Equivalent additive WGN with emprically evaluated std var. 

% Generate dictionary (system is unaware of TTD error when pre-comp dictionary)
angle_cand_num = Dict_step;
angle_cand = linspace(-pi/2,pi/2,angle_cand_num);
arx = exp(-1j*pi*(0:Nr-1)'*sin(angle_cand));
w_true = exp(-1j*2*pi*freq*((0:Nr-1) * dtau));
dict_true = w_true * arx;
% dict_true = abs(w_true * arx);

% Initialize results matrices
error_WGN = zeros(TTD_error_num, MCtimes);
error = zeros(TTD_error_num, MCtimes);

for MCidx = 1:MCtimes
    
    % Generate true AoA (close to boresight but not exactly on grid)
    true_angle = rand*2*pi/3-pi/3;

    for TTD_error_idx = 1:TTD_error_num
        
        % simulating signal with actual mismatch from TTD error
        TTD_error = randn(Nr,1) * TTD_error_range(TTD_error_idx) * 1e-12;
        w_tilde = exp(-1j*2*pi*freq*((0:Nr-1) * dtau + TTD_error'));
        arx = exp(-1j*(0:Nr-1).'*pi*sin(true_angle));
%         s1_tilde = abs(w_tilde * arx);  
        s1_tilde = w_tilde * arx;       
        score = abs(dict_true'*s1_tilde);
        [~,b] = max(score);
        error(TTD_error_idx,MCidx) = angle_cand(b) - true_angle;
        
        % simulating signal with equivalent AWGN
%         s1 = sin(Nr*pi/2*(2*freq*dtau+sin(true_angle)))./sin(pi/2*(2*freq*dtau+sin(true_angle)));
        s1 = exp(-1j*(Nr-1)*pi/2*(2*freq*dtau+sin(true_angle)))...
                .*sin(Nr*pi/2*(2 * freq * dtau+sin(true_angle)))...
                ./sin(pi/2*(2*freq*dtau+sin(true_angle))); 
        sigma = sigma_theo(TTD_error_idx);
%         s1_tilde_WGN = abs(s1) + randn(M,1) * sigma;% abs(w_tilde * arx);
        s1_tilde_WGN = s1 + (randn(M,1) + 1j*randn(M,1)) * sigma;% abs(w_tilde * arx);
        score_WGN = abs(dict_true'*s1_tilde_WGN);
        [~,b_WGN] = max(score_WGN);
        error_WGN(TTD_error_idx,MCidx) = angle_cand(b_WGN) - true_angle;

        
        % % estimator with normalization of dictionary norm (almost same norm for each columns)         
        %         for zz=1:angle_cand_num
        %             score(zz) = (dict_true(:,zz)' * s1_tilde)./norm(dict_true(:,zz))^2;
        %         end
        
    end
end

% Evaluate MSE in [deg]
MSE_WGN_deg = sqrt(mean(error_WGN.^2,2))/pi*180;
MSE_deg = sqrt(mean(error.^2,2))/pi*180;

%% Plot results
figure(99)
semilogy(TTD_error_range, MSE_deg,'-x','linewidth',2)
hold on
semilogy(TTD_error_range, MSE_WGN_deg,'-o','linewidth',2)
hold on
semilogy(TTD_error_range, final_MSE_bound,'k--','linewidth',2)
grid on
set(gca,'FontSize',14)
xlabel('RMS TTD Error [ps]')
ylabel('Angle MSE [deg]')
ylim([1e-2,100])
legend('Sim., w/ actual TTD error','Sim., w/ AWGN approx','ZZB, w/ AWGN approx')



