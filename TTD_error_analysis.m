clear;clc;

fc_num = 17;
fc_range = linspace(27.8, 28.2, fc_num)*1e9;
f0 = 28e9;
c_speed = 3e8;
lambda0 = c_speed/f0;
d_scaling = 1;
d = lambda0/2*d_scaling;

dtau_az = 1/(400e6);
% dtau_az = d/c_speed*sin(45/180*pi);
dpsi_az = pi*sin(45/180*pi)

N_az = 16;
theta_az_num = 1;
theta_az_range = -1.0803;
MCtimes = 1e3;
error_range = linspace(0,50,10)*1e-12;
gain_all = zeros(MCtimes, length(error_range));
for error_idx = 1:length(error_range)
    error_max = error_range(error_idx);
    for MCidx = 1:MCtimes
        error = (rand(N_az,1)*2-1) * error_max;

        for ff=1:fc_num
            fc = fc_range(ff);
            lambda = c_speed/fc;

    %         array_win = kaiser(N_az,3)./norm(kaiser(N_az,3))*sqrt(N_az);
            phases_fromTTD = zeros(N_az,1);
            for kk=1:N_az

                phases_fromTTD(kk) = (kk-1) * 2 * pi * fc * dtau_az...
                                    + 2 * pi * fc * error(kk);%...
%                                     - 2 * pi * f0 * error(kk);
            end

            v_az_vec(:,ff) = exp(1j*phases_fromTTD);%.*array_win;


            for tt=1:theta_az_num
                h_az(:,tt) = exp(1j*2*pi*d/lambda*(0:N_az-1).'*...
                                sin(theta_az_range(tt)))/sqrt(N_az)^2;
                gain_az_mag(ff,tt) = h_az(:,tt)'*v_az_vec(:,ff);
                gain_az(ff,tt) = abs(h_az(:,tt)'*v_az_vec(:,ff))^2;

            end
        end
        gain_all(MCidx,error_idx) = gain_az(2,1);
    end
end
%%
figure
plot(error_range/1e-12, 10*log10(mean(gain_all,1)))
hold on
xlabel('TTD Tuning Error [ps]')
ylabel('Gain [dB]')