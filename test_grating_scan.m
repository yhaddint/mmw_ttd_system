% novel beam steering method from Dr. Caire's group
clear;clc;
N = 8;
theta_range = linspace(-pi/2,pi/2,500);
BF_vec = [0,1,0,1,0,1,0,0]';
% BF_vec = [0,1,0,1,1,0,1,0,1,0]';
F_mat = zeros(N,N);
for m1=1:N
    for m2=1:N
        F_mat(m1,m2) = exp(1j*2*pi*(m1-1)*(m2-1)/N);
    end
end
array_res = exp(1j*pi*sin(theta_range)'*(0:N-1));
gain = abs(array_res * (F_mat * BF_vec)).^2;
%%
figure
polarplot(theta_range-pi,10*log10(gain),'linewidth',2)
thetalim([90,270])
rlim([0,25])
grid on