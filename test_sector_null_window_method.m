clear;
f = [0 0.2 1];
mhi = [0 1e-1 1];
bhi = fir2(64,f,mhi);
freqz(bhi,1)

%
% d = fdesign.highpass('N,Fc',16, 0.3);
% designmethods(d)

% tW = fir1(ord,[low bnd],'DC-1',tukeywin(ord+1));

% d.Passband1Constrained = true; d.Apass1 = 0.5;
% d.Passband2Constrained = true; d.Apass2 = 0.5;
% Hd = design(d,'window');

% fvtool(bhi)
%
N = length(bhi);
AWV_mag = real(DAC_quan( bhi, 8, 1 ));
figure
semilogy(abs(AWV_mag),'x','linewidth',2);
hold on
semilogy(abs(bhi),'o','linewidth',2)
grid on
%%
% AWV = [flipud(conj(Hd.Numerator(2:end).'));Hd.Numerator.'];
null_angle = -20/180*pi;
AWV = (AWV_mag.').*exp(1j*pi*sin(null_angle)*(0:N-1).');
%%
clear;clc
N = 10;
phase_ele = [0 -57.0293 -132.1477 -174.3008 -258.0691 65.3605 -18.4078 -60.5609 -135.6793 -192.7086];;

% phase_ele = [0.0946, -0.0533, -0.1036, -0.0142, 0.0414, 0.1168, -0.1533, -0.1394, -0.0199, -0.0519,...
%     -0.1240,-0.0688, 0.1669, -0.0344, -0.0313, 0.0268, -0.1067, 0.1458, 0.0952, -0.1303];
AWV = exp(-1j*phase_ele/180*pi).';
theta_num = 5e2;
theta_range = linspace(-pi/2,pi/2,theta_num);
for theta_idx = 1:theta_num
    theta = theta_range(theta_idx);
    array_res = exp(-1j * pi * sin(theta) * (0:N-1));
    gain(theta_idx) = abs(array_res * AWV);

end
%
figure
subplot(211)
plot((theta_range)/pi*180, 20*log10(gain));
% plot(asin((theta_range)*pi)/pi*180, 20*log10(gain));
grid on
xlabel('Angle')
ylabel('Response [dB]')
subplot(212)
plot(AWV,'o')
grid on
xlabel('Taps (Re)')
ylabel('Taos (Im)')
axis_lim = 0.2;
xlim([-axis_lim, axis_lim])
ylim([-axis_lim, axis_lim])

%%
