clear;clc;
N=16;
xdata = linspace(-2,2,500);
gain = sin(pi/2*N*(xdata))./sin(pi/2*xdata)/sqrt(N);
figure
plot(xdata, 20*log10(gain),'linewidth',2)
grid on
ylim([-20,10])