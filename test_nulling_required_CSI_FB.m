clear;clc
bits_range = 2:1:12;
MCtimes = 5e3;
MIMO_size = 8;
bits_num = length(bits_range);
res_pow = zeros(MCtimes, bits_num);
int_pow_scaler = [0,15,30];
figure
for int_pow_idx = 1:length(int_pow_scaler)
    for MCidx = 1:MCtimes
        H = 10*diag(exp(1j*2*pi*rand(MIMO_size,1))) + randn(MIMO_size) + 1j*randn(MIMO_size);
        H(:,1) = H(:,1)*sqrt(10^(int_pow_scaler(int_pow_idx)/10));
%         H(:,3) = H(:,3)*int_pow_scaler(int_pow_idx);
        CSI_AWGN_error = (randn(MIMO_size) + 1j*randn(MIMO_size))*sqrt(1e-3);
        for bits_idx = 1:bits_num
            bits = bits_range(bits_idx);       
            H_CSIFB = DAC_quan(H, bits, max(max(abs(H)))) + CSI_AWGN_error;
            H_final = pinv(H_CSIFB)*H;
            res_pow(MCidx, bits_idx) = abs(H_final(2,2))^2/sum(abs(H_final(2,[1,3:MIMO_size])).^2);
        end
    end

    res_pow_ave = mean(res_pow,1);
    plot(bits_range*2, 10*log10(res_pow_ave),'linewidth',2);
    hold on
end
grid on
xlabel('CSI Precision [Bits per UE/FS per Subcarrier]')
ylabel('Signal to Interference Ratio [dB]')
ylim([0,40])
legend('35dB Analog Rej.', '20dB Analog Rej.', '5dB Analog Rej.')