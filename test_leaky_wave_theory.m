clear;clc;
phi = linspace(5,90,100)/180*pi;
c = 3e8;
b_distance = [1,2,4]*10e-3;
f = c./(2*b_distance.'*sin(phi));
fmin = c./(2*b_distance.'*sin(pi/2));
BW = f-fmin;
figure
% plot(90-phi/pi*180,f/1e12,'linewidth',2)
semilogy(90-phi/pi*180,BW/1e9,'linewidth',2)

hold on
xlabel('Angle [deg]')
ylabel('BW [GHz]')
grid on
% ylim([0,0.5])
% xlim([10,85])
