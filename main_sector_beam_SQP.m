clear;clc;
N = 15;
two_d_over_lambda = 2*0.00280/0.00478;

beam_mid = 35;
bean_half_width = 4;
beam_left = beam_mid - bean_half_width*2;
beam_left = beam_mid - bean_half_width;
beam_right = beam_mid + bean_half_width;


w1_vec = exp(1j*two_d_over_lambda*pi*(1:N).'*sin(beam_mid/180*pi));
w2_vec = exp(1j*two_d_over_lambda*pi*(1:N).'*sin(beam_mid1/180*pi));
w3_vec = exp(1j*two_d_over_lambda*pi*(1:N).'*sin(beam_right/180*pi));
w4_vec = exp(1j*two_d_over_lambda*pi*(1:N).'*sin(beam_left/180*pi));

epsilon = 150;
H = zeros(N,N);
G1 = zeros(N,N);
G2 = zeros(N,N);
G3 = zeros(N,N);
HL = zeros(N,N);
A1 = zeros(N,1);
A2 = zeros(N,1);
A3 = zeros(N,1);
f = zeros(N,1);
b = 0;
x_vec(:,1) = -two_d_over_lambda*pi*(1:N).'*sin(beam_mid/180*pi);
mu1(1) = 1;
mu2(1) = 1;
mu3(1) = 1;
ite_num = 80;
% try sequentially solving the problem
for idx=1:ite_num - 1

    % H matrix in min 0.5xHx+fx
    for ii=1:N
        for jj=1:N
            if ii==jj
                H(ii,ii) = -2*2*real((-w1_vec(ii)*exp(1j*x_vec(ii,idx)))*...
                    (1+w1_vec'*exp(-1j*x_vec(:,idx)))+1);
                G1(ii,ii) = 2*real((-w2_vec(ii)*exp(1j*x_vec(ii,idx)))*...
                    (1+w2_vec'*exp(-1j*x_vec(:,idx)))+1);
                G2(ii,ii) = 2*real((-w3_vec(ii)*exp(1j*x_vec(ii,idx)))*...
                    (1+w3_vec'*exp(-1j*x_vec(:,idx)))+1);
                G3(ii,ii) = 2*real((-w4_vec(ii)*exp(1j*x_vec(ii,idx)))*...
                    (1+w4_vec'*exp(-1j*x_vec(:,idx)))+1);
            else
                H(ii,jj) = -2*2*real(w1_vec(ii)*w1_vec(jj)'*...
                    exp(1j*(x_vec(ii,idx)-x_vec(jj,idx))));
                G1(ii,jj) = 2*2*real(w2_vec(ii)*w2_vec(jj)'*...
                    exp(1j*(x_vec(ii,idx)-x_vec(jj,idx))));
                G2(ii,jj) = 2*2*real(w3_vec(ii)*w3_vec(jj)'*...
                    exp(1j*(x_vec(ii,idx)-x_vec(jj,idx))));
                G3(ii,jj) = 2*2*real(w4_vec(ii)*w4_vec(jj)'*...
                    exp(1j*(x_vec(ii,idx)-x_vec(jj,idx))));
            end

        end
    end
%     [~,Sigma,~]=svd(H);
%     diag(Sigma)

    % f vector in min 0.5xHx+fx
    % A vector in Ax<b
    for ii=1:N
        f(ii) = -2*real(1j*w1_vec(ii)*exp(1j*x_vec(ii,idx))*(1+w1_vec'*exp(-1j*x_vec(:,idx))));
        A1(ii) = 2*real(1j*w2_vec(ii)*exp(1j*x_vec(ii,idx))*(1+w2_vec'*exp(-1j*x_vec(:,idx))));
        A2(ii) = 2*real(1j*w3_vec(ii)*exp(1j*x_vec(ii,idx))*(1+w3_vec'*exp(-1j*x_vec(:,idx))));
        A3(ii) = 2*real(1j*w4_vec(ii)*exp(1j*x_vec(ii,idx))*(1+w4_vec'*exp(-1j*x_vec(:,idx))));

    end

    % b matrix in the constraint (Ax < b)
    b1 = epsilon - abs(1 + w2_vec.'*exp(1j * x_vec(:,idx)))^2;
    b2 = epsilon - abs(1 + w3_vec.'*exp(1j * x_vec(:,idx)))^2;
    b3 = epsilon - abs(1 + w4_vec.'*exp(1j * x_vec(:,idx)))^2;

    % Using Newton SQP
    HL = H + mu1(idx) * G1 + mu2(idx) * G2 + mu3(idx) * G3;
    Psi = [f + mu1(idx) * A1 + mu2(idx) * A2 + + mu3(idx) * A3; -b1; -b2; -b3];

    J = [HL, A1, A2, A3; A1.', 0, 0, 0; A2.', 0, 0, 0; A3.', 0, 0, 0];

    s = inv(J)*Psi;

    alpha = 0.2;

    mu1(idx+1) = mu1(idx) - alpha * s(end-2);
    mu2(idx+1) = mu2(idx) - alpha * s(end-1);
    mu3(idx+1) = mu3(idx) - alpha * s(end);

    d_vec = s(1:end-3);

    % QP solution and updates
%     d_vec = quadprog(H,f,A',b);
    x_vec(:,idx+1) = x_vec(:,idx) - alpha * d_vec;
end
%
w1_vec_full = [1; w1_vec];
w2_vec_full = [1; w2_vec];
w3_vec_full = [1; w3_vec];
w4_vec_full = [1; w4_vec];

x_vec_full = [zeros(1,ite_num); x_vec];
MSE = abs(w2_vec_full.'*exp(1j*x_vec_full)).^2+...
      abs(w3_vec_full.'*exp(1j*x_vec_full)).^2+...
      abs(w4_vec_full.'*exp(1j*x_vec_full)).^2;
[~,idx_to_pick] = max(MSE)

%
figure
plot(20*log10(abs(w1_vec_full.'*exp(1j*x_vec_full))),'-o','linewidth',2);
hold on
plot(20*log10(abs(w2_vec_full.'*exp(1j*x_vec_full))),'-o','linewidth',2);
hold on
plot(20*log10(abs(w3_vec_full.'*exp(1j*x_vec_full))),'-o','linewidth',2);
hold on
plot(20*log10(abs(w4_vec_full.'*exp(1j*x_vec_full))),'-o','linewidth',2);
hold on
xlabel('Iteration')
ylabel('Gain [dB]')
grid on
legend('Objective','Null-1', 'Null-2', 'Null-3')

%%
sel_idx = idx_to_pick;
stop_res = -25;
final_vec = x_vec_full(:,sel_idx);
phase6bits = round(mod(final_vec/pi*180,360)/360*(2^6));
phase6bits_new = phase6bits;
for ii=1:N+1
    if phase6bits_new(ii)==64
        phase6bits_new(ii)=0;
    end
end
final_vec_quan = phase6bits_new/64*2*pi;
final_vec_360_quan = mod(x_vec_full(:,sel_idx)/pi*180,360);
vec_dft = x_vec_full(:,1);
vec_dft2 = -two_d_over_lambda*0.95*pi*(0:N).'*sin(beam_mid/180*pi);
vec_dft3 = -two_d_over_lambda*1.05*pi*(0:N).'*sin(beam_mid/180*pi);

%
theta_range = linspace(-pi/2, pi/2, 500);
gain = zeros(500,1);
for theta_idx = 1:500
    theta = theta_range(theta_idx);
    arr_res = exp(1j*two_d_over_lambda*pi*(0:N).'*sin(theta))/sqrt(N+1);
    gain(theta_idx) = arr_res.'*exp(1j*final_vec);
    gain_quan(theta_idx) = arr_res.'*exp(1j*final_vec_quan);
    gain_dft1(theta_idx) = arr_res.'*exp(1j*vec_dft);
    gain_dft2(theta_idx) = arr_res.'*exp(1j*vec_dft2);
    gain_dft3(theta_idx) = arr_res.'*exp(1j*vec_dft3);

end

figure
subplot(211)
plot(theta_range/pi*180, 20*log10(abs(gain)),'linewidth',2);hold on
plot(theta_range/pi*180, 20*log10(abs(gain_quan)),'linewidth',2);hold on

plot(linspace(beam_left,beam_right,10), ones(10,1)*stop_res,'k-.','linewidth',1);hold on
plot(ones(10,1)*(beam_left), linspace(stop_res,5,10),'k-.','linewidth',1);hold on
plot(ones(10,1)*(beam_right), linspace(stop_res,5,10),'k-.','linewidth',1)
ylim([-40,20])
xlim([-60,60])
grid on
xlabel('Angle [deg]')
ylabel('Normalized Power Gain [dB]')
legend('Sector Null Beam (w/o Phase Quan.)','Sector Null Beam (6 bits Phase Quan.)')


subplot(212)
plot(theta_range/pi*180, 20*log10(abs(gain_dft1)),'g','linewidth',2);hold on
plot(linspace(beam_left,beam_right,10), ones(10,1)*(stop_res),'k-.','linewidth',1);hold on
plot(ones(10,1)*(beam_left), linspace(stop_res,5,10),'k-.','linewidth',1);hold on
plot(ones(10,1)*(beam_right), linspace(stop_res,5,10),'k-.','linewidth',1)
ylim([-40,20])
xlim([-60,60])
grid on
xlabel('Angle [deg]')
ylabel('Normalized Power Gain [dB]')
legend('dft beams')


%%
beam_left = beam_mid - bean_half_width - 2
blue = 	[0, 0.4470, 0.7410];
stop_res = -30;
figure(100)
% subplot(121)
polarplot(theta_range+pi,20*log10(abs(gain)),'b','linewidth',2);hold on
polarplot(linspace(beam_left,beam_right,20)/180*pi+pi,ones(20,1)*stop_res,'k--','linewidth',2);hold on
polarplot(linspace(beam_left,beam_left,20)/180*pi+pi,linspace(stop_res, 10, 20),'k--','linewidth',2);hold on
polarplot(linspace(beam_right,beam_right,20)/180*pi+pi,linspace(stop_res, 10, 20),'k--','linewidth',2);hold on
set(gca,'FontSize', 14)
% title('WB Bema Pattern')
rlim([-50,20])
% thetalim([-90 90])
thetalim([90,270])
ax = gca;
% ax.ThetaTick = -90:22.5:90;
ax.ThetaTick = 90:22.5:270;
ax.ThetaTickLabels = {'-90','-67.5','-45','-22.5','0','22.5','45','67.5','90'};
grid on
% title('WB Beam Pattern (TTD)')
% legend('all-band')

stop_res = -10;
figure(101)
% subplot(122)
polarplot(theta_range+pi,20*log10(abs(gain_dft1)),'b','linewidth',2);hold on
polarplot(theta_range+pi,20*log10(abs(gain_dft2)),'b','linewidth',2);hold on
polarplot(theta_range+pi,20*log10(abs(gain_dft3)),'b','linewidth',2);hold on

polarplot(linspace(beam_left,beam_right,20)/180*pi+pi,ones(20,1)*stop_res,'k--','linewidth',2);hold on
polarplot(linspace(beam_left,beam_left,20)/180*pi+pi,linspace(stop_res, 10, 20),'k--','linewidth',2);hold on
polarplot(linspace(beam_right,beam_right,20)/180*pi+pi,linspace(stop_res, 10, 20),'k--','linewidth',2);hold on

set(gca,'FontSize', 14)
% title('WB Beam Pattern (Phased Array)')
rlim([-50,20])
% thetalim([-90 90])
thetalim([90,270])
ax = gca;
% ax.ThetaTick = -90:22.5:90;
ax.ThetaTick = 90:22.5:270;
ax.ThetaTickLabels = {'-90','-67.5','-45','-22.5','0','22.5','45','67.5','90'};
grid on
% legend('f_1','f_2','f_3')
