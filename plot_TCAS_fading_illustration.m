clear;clc;
AoA_range = linspace(pi/2,pi/2,200);
freq_range = linspace(-1,1,200);
freq_fading1 = randn(300,1)+1j*randn(300,1);
freq_fading2 = randn(300,1)+1j*randn(300,1);

lbp_final = fir1(40, 0.025, 'low');

freq_fading_lp1 = filter(lbp_final,1,freq_fading1);
freq_fading_lp2 = filter(lbp_final,1,freq_fading2);

freq_res1 = freq_fading_lp1(21:220);
freq_res2 = freq_fading_lp2(21:220);


%%
mtx_power = ones(200,200)*1e-3;
for aa=1:200
    for ff=1:200
        if abs(aa-50)<30
            mtx_power(aa,ff) = mtx_power(aa,ff) + exp(-abs(aa-50)/3)*abs(freq_res1(ff));
        elseif abs(aa-120)<30
            mtx_power(aa,ff) = mtx_power(aa,ff) + exp(-abs(aa-120)/3)*abs(freq_res2(ff));
        end
    end
end
%%
figure
% [X, Y] = meshgrid(AoA_range, freq_range);
h = heatmap(20*log10(mtx_power'))
h.GridVisible = 'off'
xlabel = {}
for ii=1:200
    xlabel{ii} = '';
end
h.XDisplayLabels  = xlabel;
h.YDisplayLabels  = xlabel;
 h.ColorLimits = [-20 -10]

